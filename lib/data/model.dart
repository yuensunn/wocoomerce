import 'dart:convert';
import 'package:validators/validators.dart';

class Failure {
  final String message;

  Failure(this.message);
}

class OrderStatus {
  final String _value;
  const OrderStatus(this._value);
  String get value => _value;
  static const OrderStatus any = const OrderStatus("any");
  static const OrderStatus pending = const OrderStatus("pending");
  static const OrderStatus processing = const OrderStatus("processing");
  static const OrderStatus onHold = const OrderStatus("on-hold");
  static const OrderStatus completed = const OrderStatus("completed");
  static const OrderStatus cancelled = const OrderStatus("cancelled");
  static const OrderStatus refunded = const OrderStatus("refunded");
  static const OrderStatus failed = const OrderStatus("failed");
  static const OrderStatus trash = const OrderStatus("trash");
}

class ProductType {
  final String _value;
  const ProductType(this._value);
  String get value => _value;
  static const ProductType simple = const ProductType("simple");
  static const ProductType grouped = const ProductType("grouped");
  static const ProductType externaL = const ProductType("external");
  static const ProductType variable = const ProductType("variable");
  factory ProductType.fromJson(value) {
    if (value == grouped.value)
      return grouped;
    else if (value == externaL.value)
      return externaL;
    else if (value == variable.value) return variable;
    return simple;
  }
  bool operator ==(o) => (o is ProductType && value == o.value);
  int get hashCode => _value.hashCode;
  String toJson() {
    return _value;
  }
}

class Order {
  int id;
  int parentId;
  String number;
  String orderKey;
  String createdVia;
  String version;
  String status;
  String currency;
  DateTime dateCreated;
  DateTime dateCreatedGmt;
  DateTime dateModified;
  DateTime dateModifiedGmt;
  String discountTotal;
  String discountTax;
  String shippingTotal;
  String shippingTax;
  String cartTax;
  String total;
  String totalTax;
  bool pricesIncludeTax;
  int customerId;
  String customerIpAddress;
  String customerUserAgent;
  String customerNote;
  ShippingAddress billing;
  ShippingAddress shipping;
  String paymentMethod;
  String paymentMethodTitle;
  String transactionId;
  DateTime datePaid;
  DateTime datePaidGmt;
  dynamic dateCompleted;
  dynamic dateCompletedGmt;
  String cartHash;
  List<dynamic> taxLines;
  List<ShippingLine> shippingLines;
  List<LineItem> lineItems;
  List<dynamic> feeLines;
  List<dynamic> couponLines;
  List<dynamic> refunds;
  List<Metadata> metaData;
  String currencySymbol;

  Order(
      {this.id,
      this.parentId,
      this.number,
      this.orderKey,
      this.createdVia,
      this.version,
      this.status,
      this.currency,
      this.dateCreated,
      this.dateCreatedGmt,
      this.dateModified,
      this.dateModifiedGmt,
      this.discountTotal,
      this.discountTax,
      this.shippingTotal,
      this.shippingTax,
      this.cartTax,
      this.total,
      this.totalTax,
      this.pricesIncludeTax,
      this.customerId,
      this.customerIpAddress,
      this.customerUserAgent,
      this.customerNote,
      this.billing,
      this.shipping,
      this.paymentMethod,
      this.paymentMethodTitle,
      this.transactionId,
      this.datePaid,
      this.datePaidGmt,
      this.dateCompleted,
      this.dateCompletedGmt,
      this.cartHash,
      this.taxLines,
      this.shippingLines,
      this.lineItems,
      this.feeLines,
      this.couponLines,
      this.refunds,
      this.currencySymbol,
      this.metaData});

  Order copyWith({
    int id,
    int parentId,
    String number,
    String orderKey,
    String createdVia,
    String version,
    String status,
    String currency,
    DateTime dateCreated,
    DateTime dateCreatedGmt,
    DateTime dateModified,
    DateTime dateModifiedGmt,
    String discountTotal,
    String discountTax,
    String shippingTotal,
    String shippingTax,
    String cartTax,
    String total,
    String totalTax,
    bool pricesIncludeTax,
    int customerId,
    String customerIpAddress,
    String customerUserAgent,
    String customerNote,
    ShippingAddress billing,
    ShippingAddress shipping,
    String paymentMethod,
    String paymentMethodTitle,
    String transactionId,
    DateTime datePaid,
    DateTime datePaidGmt,
    dynamic dateCompleted,
    dynamic dateCompletedGmt,
    String cartHash,
    List<dynamic> taxLines,
    List<ShippingLine> shippingLines,
    List<LineItem> lineItems,
    List<dynamic> feeLines,
    List<dynamic> couponLines,
    List<dynamic> refunds,
    String currencySymbol,
    List<Metadata> metaData,
  }) =>
      Order(
        id: id ?? this.id,
        parentId: parentId ?? this.parentId,
        number: number ?? this.number,
        orderKey: orderKey ?? this.orderKey,
        createdVia: createdVia ?? this.createdVia,
        version: version ?? this.version,
        status: status ?? this.status,
        currency: currency ?? this.currency,
        dateCreated: dateCreated ?? this.dateCreated,
        dateCreatedGmt: dateCreatedGmt ?? this.dateCreatedGmt,
        dateModified: dateModified ?? this.dateModified,
        dateModifiedGmt: dateModifiedGmt ?? this.dateModifiedGmt,
        discountTotal: discountTotal ?? this.discountTotal,
        discountTax: discountTax ?? this.discountTax,
        shippingTotal: shippingTotal ?? this.shippingTotal,
        shippingTax: shippingTax ?? this.shippingTax,
        cartTax: cartTax ?? this.cartTax,
        total: total ?? this.total,
        totalTax: totalTax ?? this.totalTax,
        pricesIncludeTax: pricesIncludeTax ?? this.pricesIncludeTax,
        customerId: customerId ?? this.customerId,
        customerIpAddress: customerIpAddress ?? this.customerIpAddress,
        customerUserAgent: customerUserAgent ?? this.customerUserAgent,
        customerNote: customerNote ?? this.customerNote,
        billing: billing ?? this.billing,
        shipping: shipping ?? this.shipping,
        paymentMethod: paymentMethod ?? this.paymentMethod,
        paymentMethodTitle: paymentMethodTitle ?? this.paymentMethodTitle,
        transactionId: transactionId ?? this.transactionId,
        datePaid: datePaid ?? this.datePaid,
        datePaidGmt: datePaidGmt ?? this.datePaidGmt,
        dateCompleted: dateCompleted ?? this.dateCompleted,
        dateCompletedGmt: dateCompletedGmt ?? this.dateCompletedGmt,
        cartHash: cartHash ?? this.cartHash,
        taxLines: taxLines ?? this.taxLines,
        shippingLines: shippingLines ?? this.shippingLines,
        lineItems: lineItems ?? this.lineItems,
        feeLines: feeLines ?? this.feeLines,
        couponLines: couponLines ?? this.couponLines,
        refunds: refunds ?? this.refunds,
        currencySymbol: currencySymbol ?? this.currencySymbol,
        metaData: metaData ?? this.metaData,
      );

  factory Order.fromRawJson(String str) => Order.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Order.fromJson(Map<String, dynamic> json) => Order(
        id: json["id"] == null ? null : json["id"],
        parentId: json["parent_id"] == null ? null : json["parent_id"],
        number: json["number"] == null ? null : json["number"],
        orderKey: json["order_key"] == null ? null : json["order_key"],
        createdVia: json["created_via"] == null ? null : json["created_via"],
        version: json["version"] == null ? null : json["version"],
        status: json["status"] == null ? null : json["status"],
        currency: json["currency"] == null ? null : json["currency"],
        dateCreated: json["date_created"] == null
            ? null
            : DateTime.parse(json["date_created"]),
        dateCreatedGmt: json["date_created_gmt"] == null
            ? null
            : DateTime.parse(json["date_created_gmt"]),
        dateModified: json["date_modified"] == null
            ? null
            : DateTime.parse(json["date_modified"]),
        dateModifiedGmt: json["date_modified_gmt"] == null
            ? null
            : DateTime.parse(json["date_modified_gmt"]),
        discountTotal:
            json["discount_total"] == null ? null : json["discount_total"],
        discountTax: json["discount_tax"] == null ? null : json["discount_tax"],
        shippingTotal:
            json["shipping_total"] == null ? null : json["shipping_total"],
        shippingTax: json["shipping_tax"] == null ? null : json["shipping_tax"],
        cartTax: json["cart_tax"] == null ? null : json["cart_tax"],
        total: json["total"] == null ? null : json["total"],
        totalTax: json["total_tax"] == null ? null : json["total_tax"],
        pricesIncludeTax: json["prices_include_tax"] == null
            ? null
            : json["prices_include_tax"],
        customerId: json["customer_id"] == null ? null : json["customer_id"],
        customerIpAddress: json["customer_ip_address"] == null
            ? null
            : json["customer_ip_address"],
        customerUserAgent: json["customer_user_agent"] == null
            ? null
            : json["customer_user_agent"],
        customerNote:
            json["customer_note"] == null ? null : json["customer_note"],
        billing: json["billing"] == null
            ? null
            : ShippingAddress.fromJson(json["billing"]),
        shipping: json["shipping"] == null
            ? null
            : ShippingAddress.fromJson(json["shipping"]),
        paymentMethod:
            json["payment_method"] == null ? null : json["payment_method"],
        paymentMethodTitle: json["payment_method_title"] == null
            ? null
            : json["payment_method_title"],
        transactionId:
            json["transaction_id"] == null ? null : json["transaction_id"],
        datePaid: json["date_paid"] == null
            ? null
            : DateTime.parse(json["date_paid"]),
        datePaidGmt: json["date_paid_gmt"] == null
            ? null
            : DateTime.parse(json["date_paid_gmt"]),
        dateCompleted: json["date_completed"],
        dateCompletedGmt: json["date_completed_gmt"],
        cartHash: json["cart_hash"] == null ? null : json["cart_hash"],
        taxLines: json["tax_lines"] == null
            ? null
            : List<dynamic>.from(json["tax_lines"].map((x) => x)),
        // TODO: add shipping implementation
        // shippingLines: json["shipping_lines"] == null ? null : List<ShippingLine>.from(json["shipping_lines"].map((x) => ShippingLine.fromJson(x))),
        lineItems: json["line_items"] == null
            ? null
            : List<LineItem>.from(
                json["line_items"].map((x) => LineItem.fromJson(x))),
        feeLines: json["fee_lines"] == null
            ? null
            : List<dynamic>.from(json["fee_lines"].map((x) => x)),
        couponLines: json["coupon_lines"] == null
            ? null
            : List<dynamic>.from(json["coupon_lines"].map((x) => x)),
        refunds: json["refunds"] == null
            ? null
            : List<dynamic>.from(json["refunds"].map((x) => x)),
        currencySymbol:
            json["currency_symbol"] == null ? null : json["currency_symbol"],
        metaData: json["meta_data"] == null
            ? null
            : List<Metadata>.from(
                json["meta_data"].map((x) => Metadata.fromMap(x))),
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "parent_id": parentId == null ? null : parentId,
        "number": number == null ? null : number,
        "order_key": orderKey == null ? null : orderKey,
        "created_via": createdVia == null ? null : createdVia,
        "version": version == null ? null : version,
        "status": status == null ? null : status,
        "currency": currency == null ? null : currency,
        "date_created":
            dateCreated == null ? null : dateCreated.toIso8601String(),
        "date_created_gmt":
            dateCreatedGmt == null ? null : dateCreatedGmt.toIso8601String(),
        "date_modified":
            dateModified == null ? null : dateModified.toIso8601String(),
        "date_modified_gmt":
            dateModifiedGmt == null ? null : dateModifiedGmt.toIso8601String(),
        "discount_total": discountTotal == null ? null : discountTotal,
        "discount_tax": discountTax == null ? null : discountTax,
        "shipping_total": shippingTotal == null ? null : shippingTotal,
        "shipping_tax": shippingTax == null ? null : shippingTax,
        "cart_tax": cartTax == null ? null : cartTax,
        "total": total == null ? null : total,
        "total_tax": totalTax == null ? null : totalTax,
        "prices_include_tax":
            pricesIncludeTax == null ? null : pricesIncludeTax,
        "customer_id": customerId == null ? null : customerId,
        "customer_ip_address":
            customerIpAddress == null ? null : customerIpAddress,
        "customer_user_agent":
            customerUserAgent == null ? null : customerUserAgent,
        "customer_note": customerNote == null ? null : customerNote,
        "billing": billing == null ? null : billing.toJson(),
        "shipping": shipping == null ? null : shipping.toJson(),
        "payment_method": paymentMethod == null ? null : paymentMethod,
        "payment_method_title":
            paymentMethodTitle == null ? null : paymentMethodTitle,
        "transaction_id": transactionId == null ? null : transactionId,
        "date_paid": datePaid == null ? null : datePaid.toIso8601String(),
        "date_paid_gmt":
            datePaidGmt == null ? null : datePaidGmt.toIso8601String(),
        "date_completed": dateCompleted,
        "date_completed_gmt": dateCompletedGmt,
        "cart_hash": cartHash == null ? null : cartHash,
        "tax_lines": taxLines == null
            ? null
            : List<dynamic>.from(taxLines.map((x) => x)),
        "shipping_lines": shippingLines == null
            ? null
            : List<dynamic>.from(shippingLines.map((x) => x.toJson())),
        "line_items": lineItems == null
            ? null
            : List<dynamic>.from(lineItems.map((x) => x.toJson())),
        "fee_lines": feeLines == null
            ? null
            : List<dynamic>.from(feeLines.map((x) => x)),
        "coupon_lines": couponLines == null
            ? null
            : List<dynamic>.from(couponLines.map((x) => x)),
        "refunds":
            refunds == null ? null : List<dynamic>.from(refunds.map((x) => x)),
        "currency_symbol": currencySymbol == null ? null : currencySymbol,
        "meta_data": metaData == null
            ? []
            : List<dynamic>.from(metaData.map((x) => x.toMap()))
      };
}

class ShippingAddress {
  String firstName;
  String lastName;
  String address1;
  String address2;
  String city;
  String state;
  String postcode;
  String country;
  String email;
  String phone;

  bool get isValid =>
      firstName.isNotEmpty &&
      lastName.isNotEmpty &&
      address1.isNotEmpty &&
      city.isNotEmpty &&
      state.isNotEmpty &&
      postcode.isNotEmpty &&
      isNumeric(postcode) &&
      country.isNotEmpty &&
      isEmail(email) &&
      phone.isNotEmpty;

  ShippingAddress({
    this.firstName = "",
    this.lastName = "",
    this.address1 = "",
    this.address2 = "",
    this.city = "",
    this.state = "",
    this.postcode = "",
    this.country = "",
    this.email = "",
    this.phone = "",
  });

  ShippingAddress copyWith({
    String firstName,
    String lastName,
    String address1,
    String address2,
    String city,
    String state,
    String postcode,
    String country,
    String email,
    String phone,
  }) =>
      ShippingAddress(
        firstName: firstName ?? this.firstName,
        lastName: lastName ?? this.lastName,
        address1: address1 ?? this.address1,
        address2: address2 ?? this.address2,
        city: city ?? this.city,
        state: state ?? this.state,
        postcode: postcode ?? this.postcode,
        country: country ?? this.country,
        email: email ?? this.email,
        phone: phone ?? this.phone,
      );

  factory ShippingAddress.fromRawJson(String str) =>
      ShippingAddress.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory ShippingAddress.fromJson(Map<String, dynamic> json) =>
      ShippingAddress(
        firstName: json["first_name"] == null ? null : json["first_name"],
        lastName: json["last_name"] == null ? null : json["last_name"],
        address1: json["address_1"] == null ? null : json["address_1"],
        address2: json["address_2"] == null ? null : json["address_2"],
        city: json["city"] == null ? null : json["city"],
        state: json["state"] == null ? null : json["state"],
        postcode: json["postcode"] == null ? null : json["postcode"],
        country: json["country"] == null ? null : json["country"],
        email: json["email"] == null ? null : json["email"],
        phone: json["phone"] == null ? null : json["phone"],
      );

  Map<String, dynamic> toJson() => {
        "first_name": firstName == null ? null : firstName,
        "last_name": lastName == null ? null : lastName,
        "address_1": address1 == null ? null : address1,
        "address_2": address2 == null ? null : address2,
        "city": city == null ? null : city,
        "state": state == null ? null : state,
        if (postcode != null && postcode.isNotEmpty)
          "postcode": postcode == null ? null : postcode,
        "country": country == null ? null : country,
        if (email != null && email.isNotEmpty) "email": email,
        if (phone != null && phone.isNotEmpty)
          "phone": phone == null ? null : phone,
      };
}

class LineItem {
  int productId;
  int quantity;
  int variationId;

  LineItem({
    this.productId,
    this.quantity,
    this.variationId,
  });

  LineItem copyWith({
    int productId,
    int quantity,
    int variationId,
  }) =>
      LineItem(
        productId: productId ?? this.productId,
        quantity: quantity ?? this.quantity,
        variationId: variationId ?? this.variationId,
      );

  factory LineItem.fromRawJson(String str) =>
      LineItem.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory LineItem.fromJson(Map<String, dynamic> json) => LineItem(
        productId: json["product_id"] == null ? null : json["product_id"],
        quantity: json["quantity"] == null ? null : json["quantity"],
        variationId: json["variation_id"] == null ? null : json["variation_id"],
      );

  Map<String, dynamic> toJson() => {
        "product_id": productId == null ? null : productId,
        "quantity": quantity == null ? null : quantity,
        if (variationId != null) "variation_id": variationId,
      };
}

class ShippingLine {
  String methodId;
  String methodTitle;
  int total;

  ShippingLine({
    this.methodId,
    this.methodTitle,
    this.total,
  });

  ShippingLine copyWith({
    String methodId,
    String methodTitle,
    int total,
  }) =>
      ShippingLine(
        methodId: methodId ?? this.methodId,
        methodTitle: methodTitle ?? this.methodTitle,
        total: total ?? this.total,
      );

  factory ShippingLine.fromRawJson(String str) =>
      ShippingLine.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory ShippingLine.fromJson(Map<String, dynamic> json) => ShippingLine(
        methodId: json["method_id"] == null ? null : json["method_id"],
        methodTitle: json["method_title"] == null ? null : json["method_title"],
        //TODO: add shipping fees
        // total: json["total"] == null ? null : json["total"],
      );

  Map<String, dynamic> toJson() => {
        "method_id": methodId == null ? null : methodId,
        "method_title": methodTitle == null ? null : methodTitle,
        //TODO: add shipping fees
        // if (total != null &&  total.is) "total": total == null ? null : total,
      };
}

class ShippingMethod {
  String id;
  String title;
  String description;

  ShippingMethod({
    this.id,
    this.title,
    this.description,
  });

  ShippingMethod copyWith({
    String id,
    String title,
    String description,
  }) =>
      ShippingMethod(
        id: id ?? this.id,
        title: title ?? this.title,
        description: description ?? this.description,
      );

  factory ShippingMethod.fromRawJson(String str) =>
      ShippingMethod.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory ShippingMethod.fromJson(Map<String, dynamic> json) => ShippingMethod(
        id: json["id"] == null ? null : json["id"],
        title: json["title"] == null ? null : json["title"],
        description: json["description"] == null ? null : json["description"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "title": title == null ? null : title,
        "description": description == null ? null : description,
      };
}

class ShippingZoneMethod {
  int id;
  int instanceId;
  String title;
  int order;
  bool enabled;
  String methodId;
  String methodTitle;
  String methodDescription;

  ShippingZoneMethod({
    this.id,
    this.instanceId,
    this.title,
    this.order,
    this.enabled,
    this.methodId,
    this.methodTitle,
    this.methodDescription,
  });

  ShippingZoneMethod copyWith({
    int id,
    int instanceId,
    String title,
    int order,
    bool enabled,
    String methodId,
    String methodTitle,
    String methodDescription,
  }) =>
      ShippingZoneMethod(
        id: id ?? this.id,
        instanceId: instanceId ?? this.instanceId,
        title: title ?? this.title,
        order: order ?? this.order,
        enabled: enabled ?? this.enabled,
        methodId: methodId ?? this.methodId,
        methodTitle: methodTitle ?? this.methodTitle,
        methodDescription: methodDescription ?? this.methodDescription,
      );

  factory ShippingZoneMethod.fromRawJson(String str) =>
      ShippingZoneMethod.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory ShippingZoneMethod.fromJson(Map<String, dynamic> json) =>
      ShippingZoneMethod(
        id: json["id"] == null ? null : json["id"],
        instanceId: json["instance_id"] == null ? null : json["instance_id"],
        title: json["title"] == null ? null : json["title"],
        order: json["order"] == null ? null : json["order"],
        enabled: json["enabled"] == null ? null : json["enabled"],
        methodId: json["method_id"] == null ? null : json["method_id"],
        methodTitle: json["method_title"] == null ? null : json["method_title"],
        methodDescription: json["method_description"] == null
            ? null
            : json["method_description"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "instance_id": instanceId == null ? null : instanceId,
        "title": title == null ? null : title,
        "order": order == null ? null : order,
        "enabled": enabled == null ? null : enabled,
        "method_id": methodId == null ? null : methodId,
        "method_title": methodTitle == null ? null : methodTitle,
        "method_description":
            methodDescription == null ? null : methodDescription,
      };
}

class Product {
  int id;
  String name;
  String slug;
  String permalink;
  DateTime dateCreated;
  DateTime dateCreatedGmt;
  DateTime dateModified;
  DateTime dateModifiedGmt;
  ProductType type;
  String status;
  bool featured;
  String catalogVisibility;
  String description;
  String shortDescription;
  String sku;
  String price;
  String regularPrice;
  String salePrice;
  dynamic dateOnSaleFrom;
  dynamic dateOnSaleFromGmt;
  dynamic dateOnSaleTo;
  dynamic dateOnSaleToGmt;
  String priceHtml;
  bool onSale;
  bool purchasable;
  int totalSales;
  bool virtual;
  bool downloadable;
  List<dynamic> downloads;
  int downloadLimit;
  int downloadExpiry;
  String externalUrl;
  String buttonText;
  String taxStatus;
  String taxClass;
  bool manageStock;
  dynamic stockQuantity;
  String stockStatus;
  String backorders;
  bool backordersAllowed;
  bool backordered;
  bool soldIndividually;
  String weight;
  Dimensions dimensions;
  bool shippingRequired;
  bool shippingTaxable;
  String shippingClass;
  int shippingClassId;
  bool reviewsAllowed;
  String averageRating;
  int ratingCount;
  List<int> relatedIds;
  List<dynamic> upsellIds;
  List<dynamic> crossSellIds;
  int parentId;
  String purchaseNote;
  List<Category> categories;
  List<dynamic> tags;
  List<Image> images;
  List<Attribute> attributes;
  List<dynamic> defaultAttributes;
  List<int> variations;
  List<dynamic> groupedProducts;
  int menuOrder;
  int supplierId;
  String supplierSku;
  bool atumControlled;
  dynamic outStockDate;
  int outStockThreshold;
  bool inheritable;
  dynamic inboundStock;
  int stockOnHold;
  int soldToday;
  int salesLastDays;
  dynamic reservedStock;
  dynamic customerReturns;
  dynamic warehouseDamage;
  dynamic lostInPost;
  dynamic otherLogs;
  int outStockDays;
  int lostSales;
  bool hasLocation;
  dynamic updateDate;
  List<dynamic> atumLocations;
  List<dynamic> acf;

  Product({
    this.id,
    this.name,
    this.slug,
    this.permalink,
    this.dateCreated,
    this.dateCreatedGmt,
    this.dateModified,
    this.dateModifiedGmt,
    this.type,
    this.status,
    this.featured,
    this.catalogVisibility,
    this.description,
    this.shortDescription,
    this.sku,
    this.price,
    this.regularPrice,
    this.salePrice,
    this.dateOnSaleFrom,
    this.dateOnSaleFromGmt,
    this.dateOnSaleTo,
    this.dateOnSaleToGmt,
    this.priceHtml,
    this.onSale,
    this.purchasable,
    this.totalSales,
    this.virtual,
    this.downloadable,
    this.downloads,
    this.downloadLimit,
    this.downloadExpiry,
    this.externalUrl,
    this.buttonText,
    this.taxStatus,
    this.taxClass,
    this.manageStock,
    this.stockQuantity,
    this.stockStatus,
    this.backorders,
    this.backordersAllowed,
    this.backordered,
    this.soldIndividually,
    this.weight,
    this.dimensions,
    this.shippingRequired,
    this.shippingTaxable,
    this.shippingClass,
    this.shippingClassId,
    this.reviewsAllowed,
    this.averageRating,
    this.ratingCount,
    this.relatedIds,
    this.upsellIds,
    this.crossSellIds,
    this.parentId,
    this.purchaseNote,
    this.categories,
    this.tags,
    this.images,
    this.attributes,
    this.defaultAttributes,
    this.variations,
    this.groupedProducts,
    this.menuOrder,
    this.supplierId,
    this.supplierSku,
    this.atumControlled,
    this.outStockDate,
    this.outStockThreshold,
    this.inheritable,
    this.inboundStock,
    this.stockOnHold,
    this.soldToday,
    this.salesLastDays,
    this.reservedStock,
    this.customerReturns,
    this.warehouseDamage,
    this.lostInPost,
    this.otherLogs,
    this.outStockDays,
    this.lostSales,
    this.hasLocation,
    this.updateDate,
    this.atumLocations,
    this.acf,
  });

  Product copyWith({
    int id,
    String name,
    String slug,
    String permalink,
    DateTime dateCreated,
    DateTime dateCreatedGmt,
    DateTime dateModified,
    DateTime dateModifiedGmt,
    String type,
    String status,
    bool featured,
    String catalogVisibility,
    String description,
    String shortDescription,
    String sku,
    String price,
    String regularPrice,
    String salePrice,
    dynamic dateOnSaleFrom,
    dynamic dateOnSaleFromGmt,
    dynamic dateOnSaleTo,
    dynamic dateOnSaleToGmt,
    String priceHtml,
    bool onSale,
    bool purchasable,
    int totalSales,
    bool virtual,
    bool downloadable,
    List<dynamic> downloads,
    int downloadLimit,
    int downloadExpiry,
    String externalUrl,
    String buttonText,
    String taxStatus,
    String taxClass,
    bool manageStock,
    dynamic stockQuantity,
    String stockStatus,
    String backorders,
    bool backordersAllowed,
    bool backordered,
    bool soldIndividually,
    String weight,
    Dimensions dimensions,
    bool shippingRequired,
    bool shippingTaxable,
    String shippingClass,
    int shippingClassId,
    bool reviewsAllowed,
    String averageRating,
    int ratingCount,
    List<int> relatedIds,
    List<dynamic> upsellIds,
    List<dynamic> crossSellIds,
    int parentId,
    String purchaseNote,
    List<Category> categories,
    List<dynamic> tags,
    List<Image> images,
    List<Attribute> attributes,
    List<dynamic> defaultAttributes,
    List<int> variations,
    List<dynamic> groupedProducts,
    int menuOrder,
    int supplierId,
    String supplierSku,
    bool atumControlled,
    dynamic outStockDate,
    int outStockThreshold,
    bool inheritable,
    dynamic inboundStock,
    int stockOnHold,
    int soldToday,
    int salesLastDays,
    dynamic reservedStock,
    dynamic customerReturns,
    dynamic warehouseDamage,
    dynamic lostInPost,
    dynamic otherLogs,
    int outStockDays,
    int lostSales,
    bool hasLocation,
    dynamic updateDate,
    List<dynamic> atumLocations,
    List<dynamic> acf,
  }) =>
      Product(
        id: id ?? this.id,
        name: name ?? this.name,
        slug: slug ?? this.slug,
        permalink: permalink ?? this.permalink,
        dateCreated: dateCreated ?? this.dateCreated,
        dateCreatedGmt: dateCreatedGmt ?? this.dateCreatedGmt,
        dateModified: dateModified ?? this.dateModified,
        dateModifiedGmt: dateModifiedGmt ?? this.dateModifiedGmt,
        type: type ?? this.type,
        status: status ?? this.status,
        featured: featured ?? this.featured,
        catalogVisibility: catalogVisibility ?? this.catalogVisibility,
        description: description ?? this.description,
        shortDescription: shortDescription ?? this.shortDescription,
        sku: sku ?? this.sku,
        price: price ?? this.price,
        regularPrice: regularPrice ?? this.regularPrice,
        salePrice: salePrice ?? this.salePrice,
        dateOnSaleFrom: dateOnSaleFrom ?? this.dateOnSaleFrom,
        dateOnSaleFromGmt: dateOnSaleFromGmt ?? this.dateOnSaleFromGmt,
        dateOnSaleTo: dateOnSaleTo ?? this.dateOnSaleTo,
        dateOnSaleToGmt: dateOnSaleToGmt ?? this.dateOnSaleToGmt,
        priceHtml: priceHtml ?? this.priceHtml,
        onSale: onSale ?? this.onSale,
        purchasable: purchasable ?? this.purchasable,
        totalSales: totalSales ?? this.totalSales,
        virtual: virtual ?? this.virtual,
        downloadable: downloadable ?? this.downloadable,
        downloads: downloads ?? this.downloads,
        downloadLimit: downloadLimit ?? this.downloadLimit,
        downloadExpiry: downloadExpiry ?? this.downloadExpiry,
        externalUrl: externalUrl ?? this.externalUrl,
        buttonText: buttonText ?? this.buttonText,
        taxStatus: taxStatus ?? this.taxStatus,
        taxClass: taxClass ?? this.taxClass,
        manageStock: manageStock ?? this.manageStock,
        stockQuantity: stockQuantity ?? this.stockQuantity,
        stockStatus: stockStatus ?? this.stockStatus,
        backorders: backorders ?? this.backorders,
        backordersAllowed: backordersAllowed ?? this.backordersAllowed,
        backordered: backordered ?? this.backordered,
        soldIndividually: soldIndividually ?? this.soldIndividually,
        weight: weight ?? this.weight,
        dimensions: dimensions ?? this.dimensions,
        shippingRequired: shippingRequired ?? this.shippingRequired,
        shippingTaxable: shippingTaxable ?? this.shippingTaxable,
        shippingClass: shippingClass ?? this.shippingClass,
        shippingClassId: shippingClassId ?? this.shippingClassId,
        reviewsAllowed: reviewsAllowed ?? this.reviewsAllowed,
        averageRating: averageRating ?? this.averageRating,
        ratingCount: ratingCount ?? this.ratingCount,
        relatedIds: relatedIds ?? this.relatedIds,
        upsellIds: upsellIds ?? this.upsellIds,
        crossSellIds: crossSellIds ?? this.crossSellIds,
        parentId: parentId ?? this.parentId,
        purchaseNote: purchaseNote ?? this.purchaseNote,
        categories: categories ?? this.categories,
        tags: tags ?? this.tags,
        images: images ?? this.images,
        attributes: attributes ?? this.attributes,
        defaultAttributes: defaultAttributes ?? this.defaultAttributes,
        variations: variations ?? this.variations,
        groupedProducts: groupedProducts ?? this.groupedProducts,
        menuOrder: menuOrder ?? this.menuOrder,
        supplierId: supplierId ?? this.supplierId,
        supplierSku: supplierSku ?? this.supplierSku,
        atumControlled: atumControlled ?? this.atumControlled,
        outStockDate: outStockDate ?? this.outStockDate,
        outStockThreshold: outStockThreshold ?? this.outStockThreshold,
        inheritable: inheritable ?? this.inheritable,
        inboundStock: inboundStock ?? this.inboundStock,
        stockOnHold: stockOnHold ?? this.stockOnHold,
        soldToday: soldToday ?? this.soldToday,
        salesLastDays: salesLastDays ?? this.salesLastDays,
        reservedStock: reservedStock ?? this.reservedStock,
        customerReturns: customerReturns ?? this.customerReturns,
        warehouseDamage: warehouseDamage ?? this.warehouseDamage,
        lostInPost: lostInPost ?? this.lostInPost,
        otherLogs: otherLogs ?? this.otherLogs,
        outStockDays: outStockDays ?? this.outStockDays,
        lostSales: lostSales ?? this.lostSales,
        hasLocation: hasLocation ?? this.hasLocation,
        updateDate: updateDate ?? this.updateDate,
        atumLocations: atumLocations ?? this.atumLocations,
        acf: acf ?? this.acf,
      );

  factory Product.fromRawJson(String str) => Product.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Product.fromJson(Map<String, dynamic> json) => Product(
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? null : json["name"],
        slug: json["slug"] == null ? null : json["slug"],
        permalink: json["permalink"] == null ? null : json["permalink"],
        dateCreated: json["date_created"] == null
            ? null
            : DateTime.parse(json["date_created"]),
        dateCreatedGmt: json["date_created_gmt"] == null
            ? null
            : DateTime.parse(json["date_created_gmt"]),
        dateModified: json["date_modified"] == null
            ? null
            : DateTime.parse(json["date_modified"]),
        dateModifiedGmt: json["date_modified_gmt"] == null
            ? null
            : DateTime.parse(json["date_modified_gmt"]),
        type: json["type"] == null ? null : ProductType.fromJson(json["type"]),
        status: json["status"] == null ? null : json["status"],
        featured: json["featured"] == null ? null : json["featured"],
        catalogVisibility: json["catalog_visibility"] == null
            ? null
            : json["catalog_visibility"],
        description: json["description"] == null ? null : json["description"],
        shortDescription: json["short_description"] == null
            ? null
            : json["short_description"],
        sku: json["sku"] == null ? null : json["sku"],
        price: json["price"] == null ? null : json["price"],
        regularPrice:
            json["regular_price"] == null ? null : json["regular_price"],
        salePrice: json["sale_price"] == null ? null : json["sale_price"],
        dateOnSaleFrom: json["date_on_sale_from"],
        dateOnSaleFromGmt: json["date_on_sale_from_gmt"],
        dateOnSaleTo: json["date_on_sale_to"],
        dateOnSaleToGmt: json["date_on_sale_to_gmt"],
        priceHtml: json["price_html"] == null ? null : json["price_html"],
        onSale: json["on_sale"] == null ? null : json["on_sale"],
        purchasable: json["purchasable"] == null ? null : json["purchasable"],
        totalSales: json["total_sales"] == null
            ? 0
            : (json["total_sales"] is int)
                ? json["total_sales"]
                : int.parse(json["total_sales"]),
        virtual: json["virtual"] == null ? null : json["virtual"],
        downloadable:
            json["downloadable"] == null ? null : json["downloadable"],
        downloads: json["downloads"] == null
            ? null
            : List<dynamic>.from(json["downloads"].map((x) => x)),
        downloadLimit:
            json["download_limit"] == null ? null : json["download_limit"],
        downloadExpiry:
            json["download_expiry"] == null ? null : json["download_expiry"],
        externalUrl: json["external_url"] == null ? null : json["external_url"],
        buttonText: json["button_text"] == null ? null : json["button_text"],
        taxStatus: json["tax_status"] == null ? null : json["tax_status"],
        taxClass: json["tax_class"] == null ? null : json["tax_class"],
        manageStock: json["manage_stock"] == null ? null : json["manage_stock"],
        stockQuantity: json["stock_quantity"],
        stockStatus: json["stock_status"] == null ? null : json["stock_status"],
        backorders: json["backorders"] == null ? null : json["backorders"],
        backordersAllowed: json["backorders_allowed"] == null
            ? null
            : json["backorders_allowed"],
        backordered: json["backordered"] == null ? null : json["backordered"],
        soldIndividually: json["sold_individually"] == null
            ? null
            : json["sold_individually"],
        weight: json["weight"] == null ? null : json["weight"],
        dimensions: json["dimensions"] == null
            ? null
            : Dimensions.fromJson(json["dimensions"]),
        shippingRequired: json["shipping_required"] == null
            ? null
            : json["shipping_required"],
        shippingTaxable:
            json["shipping_taxable"] == null ? null : json["shipping_taxable"],
        shippingClass:
            json["shipping_class"] == null ? null : json["shipping_class"],
        shippingClassId: json["shipping_class_id"] == null
            ? null
            : json["shipping_class_id"],
        reviewsAllowed:
            json["reviews_allowed"] == null ? null : json["reviews_allowed"],
        averageRating:
            json["average_rating"] == null ? null : json["average_rating"],
        ratingCount: json["rating_count"] == null ? null : json["rating_count"],
        relatedIds: json["related_ids"] == null
            ? null
            : List<int>.from(json["related_ids"].map((x) => x)),
        upsellIds: json["upsell_ids"] == null
            ? null
            : List<dynamic>.from(json["upsell_ids"].map((x) => x)),
        crossSellIds: json["cross_sell_ids"] == null
            ? null
            : List<dynamic>.from(json["cross_sell_ids"].map((x) => x)),
        parentId: json["parent_id"] == null ? null : json["parent_id"],
        purchaseNote:
            json["purchase_note"] == null ? null : json["purchase_note"],
        categories: json["categories"] == null
            ? null
            : List<Category>.from(
                json["categories"].map((x) => Category.fromJson(x))),
        tags: json["tags"] == null
            ? null
            : List<dynamic>.from(json["tags"].map((x) => x)),
        images: json["images"] == null
            ? null
            : List<Image>.from(json["images"].map((x) => Image.fromJson(x))),
        attributes: json["attributes"] == null
            ? null
            : List<Attribute>.from(
                json["attributes"].map((x) => Attribute.fromJson(x))),
        defaultAttributes: json["default_attributes"] == null
            ? null
            : List<dynamic>.from(json["default_attributes"].map((x) => x)),
        variations: json["variations"] == null
            ? null
            : List<int>.from(json["variations"].map((x) => x)),
        groupedProducts: json["grouped_products"] == null
            ? null
            : List<dynamic>.from(json["grouped_products"].map((x) => x)),
        menuOrder: json["menu_order"] == null ? null : json["menu_order"],
        supplierId: json["supplier_id"] == null ? null : json["supplier_id"],
        supplierSku: json["supplier_sku"] == null ? null : json["supplier_sku"],
        atumControlled:
            json["atum_controlled"] == null ? null : json["atum_controlled"],
        outStockDate: json["out_stock_date"],
        outStockThreshold: json["out_stock_threshold"] == null
            ? null
            : json["out_stock_threshold"],
        inheritable: json["inheritable"] == null ? null : json["inheritable"],
        inboundStock: json["inbound_stock"],
        stockOnHold:
            json["stock_on_hold"] == null ? null : json["stock_on_hold"],
        soldToday: json["sold_today"] == null ? null : json["sold_today"],
        salesLastDays:
            json["sales_last_days"] == null ? null : json["sales_last_days"],
        reservedStock: json["reserved_stock"],
        customerReturns: json["customer_returns"],
        warehouseDamage: json["warehouse_damage"],
        lostInPost: json["lost_in_post"],
        otherLogs: json["other_logs"],
        outStockDays:
            json["out_stock_days"] == null ? null : json["out_stock_days"],
        lostSales: json["lost_sales"] == null ? null : json["lost_sales"],
        hasLocation: json["has_location"] == null ? null : json["has_location"],
        updateDate: json["update_date"],
        atumLocations: json["atum_locations"] == null
            ? null
            : List<dynamic>.from(json["atum_locations"].map((x) => x)),
        acf: json["acf"] == null
            ? null
            : List<dynamic>.from(json["acf"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "name": name == null ? null : name,
        "slug": slug == null ? null : slug,
        "permalink": permalink == null ? null : permalink,
        "date_created":
            dateCreated == null ? null : dateCreated.toIso8601String(),
        "date_created_gmt":
            dateCreatedGmt == null ? null : dateCreatedGmt.toIso8601String(),
        "date_modified":
            dateModified == null ? null : dateModified.toIso8601String(),
        "date_modified_gmt":
            dateModifiedGmt == null ? null : dateModifiedGmt.toIso8601String(),
        "type": type == null ? null : type.toJson(),
        "status": status == null ? null : status,
        "featured": featured == null ? null : featured,
        "catalog_visibility":
            catalogVisibility == null ? null : catalogVisibility,
        "description": description == null ? null : description,
        "short_description": shortDescription == null ? null : shortDescription,
        "sku": sku == null ? null : sku,
        "price": price == null ? null : price,
        "regular_price": regularPrice == null ? null : regularPrice,
        "sale_price": salePrice == null ? null : salePrice,
        "date_on_sale_from": dateOnSaleFrom,
        "date_on_sale_from_gmt": dateOnSaleFromGmt,
        "date_on_sale_to": dateOnSaleTo,
        "date_on_sale_to_gmt": dateOnSaleToGmt,
        "price_html": priceHtml == null ? null : priceHtml,
        "on_sale": onSale == null ? null : onSale,
        "purchasable": purchasable == null ? null : purchasable,
        "total_sales": totalSales == null ? null : totalSales,
        "virtual": virtual == null ? null : virtual,
        "downloadable": downloadable == null ? null : downloadable,
        "downloads": downloads == null
            ? null
            : List<dynamic>.from(downloads.map((x) => x)),
        "download_limit": downloadLimit == null ? null : downloadLimit,
        "download_expiry": downloadExpiry == null ? null : downloadExpiry,
        "external_url": externalUrl == null ? null : externalUrl,
        "button_text": buttonText == null ? null : buttonText,
        "tax_status": taxStatus == null ? null : taxStatus,
        "tax_class": taxClass == null ? null : taxClass,
        "manage_stock": manageStock == null ? null : manageStock,
        "stock_quantity": stockQuantity,
        "stock_status": stockStatus == null ? null : stockStatus,
        "backorders": backorders == null ? null : backorders,
        "backorders_allowed":
            backordersAllowed == null ? null : backordersAllowed,
        "backordered": backordered == null ? null : backordered,
        "sold_individually": soldIndividually == null ? null : soldIndividually,
        "weight": weight == null ? null : weight,
        "dimensions": dimensions == null ? null : dimensions.toJson(),
        "shipping_required": shippingRequired == null ? null : shippingRequired,
        "shipping_taxable": shippingTaxable == null ? null : shippingTaxable,
        "shipping_class": shippingClass == null ? null : shippingClass,
        "shipping_class_id": shippingClassId == null ? null : shippingClassId,
        "reviews_allowed": reviewsAllowed == null ? null : reviewsAllowed,
        "average_rating": averageRating == null ? null : averageRating,
        "rating_count": ratingCount == null ? null : ratingCount,
        "related_ids": relatedIds == null
            ? null
            : List<dynamic>.from(relatedIds.map((x) => x)),
        "upsell_ids": upsellIds == null
            ? null
            : List<dynamic>.from(upsellIds.map((x) => x)),
        "cross_sell_ids": crossSellIds == null
            ? null
            : List<dynamic>.from(crossSellIds.map((x) => x)),
        "parent_id": parentId == null ? null : parentId,
        "purchase_note": purchaseNote == null ? null : purchaseNote,
        "categories": categories == null
            ? null
            : List<dynamic>.from(categories.map((x) => x.toJson())),
        "tags": tags == null ? null : List<dynamic>.from(tags.map((x) => x)),
        "images": images == null
            ? null
            : List<dynamic>.from(images.map((x) => x.toJson())),
        "attributes": attributes == null
            ? null
            : List<dynamic>.from(attributes.map((x) => x.toJson())),
        "default_attributes": defaultAttributes == null
            ? null
            : List<dynamic>.from(defaultAttributes.map((x) => x)),
        "variations": variations == null
            ? null
            : List<dynamic>.from(variations.map((x) => x)),
        "grouped_products": groupedProducts == null
            ? null
            : List<dynamic>.from(groupedProducts.map((x) => x)),
        "menu_order": menuOrder == null ? null : menuOrder,
        "supplier_id": supplierId == null ? null : supplierId,
        "supplier_sku": supplierSku == null ? null : supplierSku,
        "atum_controlled": atumControlled == null ? null : atumControlled,
        "out_stock_date": outStockDate,
        "out_stock_threshold":
            outStockThreshold == null ? null : outStockThreshold,
        "inheritable": inheritable == null ? null : inheritable,
        "inbound_stock": inboundStock,
        "stock_on_hold": stockOnHold == null ? null : stockOnHold,
        "sold_today": soldToday == null ? null : soldToday,
        "sales_last_days": salesLastDays == null ? null : salesLastDays,
        "reserved_stock": reservedStock,
        "customer_returns": customerReturns,
        "warehouse_damage": warehouseDamage,
        "lost_in_post": lostInPost,
        "other_logs": otherLogs,
        "out_stock_days": outStockDays == null ? null : outStockDays,
        "lost_sales": lostSales == null ? null : lostSales,
        "has_location": hasLocation == null ? null : hasLocation,
        "update_date": updateDate,
        "atum_locations": atumLocations == null
            ? null
            : List<dynamic>.from(atumLocations.map((x) => x)),
        "acf": acf == null ? null : List<dynamic>.from(acf.map((x) => x)),
      };
}

class Attribute {
  int id;
  String name;
  int position;
  bool visible;
  bool variation;
  String option;
  List<String> options;

  Attribute({
    this.id,
    this.name,
    this.position,
    this.visible,
    this.variation,
    this.option,
    this.options,
  });

  Attribute copyWith({
    int id,
    String name,
    int position,
    bool visible,
    bool variation,
    String option,
    List<String> options,
  }) =>
      Attribute(
        id: id ?? this.id,
        name: name ?? this.name,
        position: position ?? this.position,
        visible: visible ?? this.visible,
        variation: variation ?? this.variation,
        option: option ?? this.option,
        options: options ?? this.options,
      );

  factory Attribute.fromRawJson(String str) =>
      Attribute.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Attribute.fromJson(Map<String, dynamic> json) => Attribute(
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? null : json["name"],
        position: json["position"] == null ? null : json["position"],
        visible: json["visible"] == null ? null : json["visible"],
        variation: json["variation"] == null ? null : json["variation"],
        option: json["option"] == null ? null : json["option"],
        options: json["options"] == null
            ? null
            : List<String>.from(json["options"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "name": name == null ? null : name,
        "position": position == null ? null : position,
        "visible": visible == null ? null : visible,
        "variation": variation == null ? null : variation,
        "option": option == null ? null : option,
        "options":
            options == null ? null : List<dynamic>.from(options.map((x) => x)),
      };
}

class Category {
  int id;
  String name;
  String slug;
  int count;
  Image image;

  Category({this.id, this.name, this.slug, this.count, this.image});

  Category copyWith({int id, String name, String slug, int count}) => Category(
        id: id ?? this.id,
        name: name ?? this.name,
        slug: slug ?? this.slug,
        count: count ?? this.count,
        image: image ?? this.image,
      );

  factory Category.fromRawJson(String str) =>
      Category.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Category.fromJson(Map<String, dynamic> json) => Category(
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? null : json["name"],
        slug: json["slug"] == null ? null : json["slug"],
        count: json["slug"] == null ? null : json["count"],
        image: json["image"] == null ? null : Image.fromJson(json["image"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "name": name == null ? null : name,
        "slug": slug == null ? null : slug,
        "count": count == null ? null : count,
        "image": image == null ? null : image.toJson()
      };
}

class Dimensions {
  String length;
  String width;
  String height;

  Dimensions({
    this.length,
    this.width,
    this.height,
  });

  Dimensions copyWith({
    String length,
    String width,
    String height,
  }) =>
      Dimensions(
        length: length ?? this.length,
        width: width ?? this.width,
        height: height ?? this.height,
      );

  factory Dimensions.fromRawJson(String str) =>
      Dimensions.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Dimensions.fromJson(Map<String, dynamic> json) => Dimensions(
        length: json["length"] == null ? null : json["length"],
        width: json["width"] == null ? null : json["width"],
        height: json["height"] == null ? null : json["height"],
      );

  Map<String, dynamic> toJson() => {
        "length": length == null ? null : length,
        "width": width == null ? null : width,
        "height": height == null ? null : height,
      };
}

class Image {
  int id;
  DateTime dateCreated;
  DateTime dateCreatedGmt;
  DateTime dateModified;
  DateTime dateModifiedGmt;
  String src;
  String name;
  String alt;

  Image({
    this.id,
    this.dateCreated,
    this.dateCreatedGmt,
    this.dateModified,
    this.dateModifiedGmt,
    this.src,
    this.name,
    this.alt,
  });

  Image copyWith({
    int id,
    DateTime dateCreated,
    DateTime dateCreatedGmt,
    DateTime dateModified,
    DateTime dateModifiedGmt,
    String src,
    String name,
    String alt,
  }) =>
      Image(
        id: id ?? this.id,
        dateCreated: dateCreated ?? this.dateCreated,
        dateCreatedGmt: dateCreatedGmt ?? this.dateCreatedGmt,
        dateModified: dateModified ?? this.dateModified,
        dateModifiedGmt: dateModifiedGmt ?? this.dateModifiedGmt,
        src: src ?? this.src,
        name: name ?? this.name,
        alt: alt ?? this.alt,
      );

  factory Image.fromRawJson(String str) => Image.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Image.fromJson(Map<String, dynamic> json) => Image(
        id: json["id"] == null ? null : json["id"],
        dateCreated: json["date_created"] == null
            ? null
            : DateTime.parse(json["date_created"]),
        dateCreatedGmt: json["date_created_gmt"] == null
            ? null
            : DateTime.parse(json["date_created_gmt"]),
        dateModified: json["date_modified"] == null
            ? null
            : DateTime.parse(json["date_modified"]),
        dateModifiedGmt: json["date_modified_gmt"] == null
            ? null
            : DateTime.parse(json["date_modified_gmt"]),
        src: json["src"] == null ? null : json["src"],
        name: json["name"] == null ? null : json["name"],
        alt: json["alt"] == null ? null : json["alt"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "date_created":
            dateCreated == null ? null : dateCreated.toIso8601String(),
        "date_created_gmt":
            dateCreatedGmt == null ? null : dateCreatedGmt.toIso8601String(),
        "date_modified":
            dateModified == null ? null : dateModified.toIso8601String(),
        "date_modified_gmt":
            dateModifiedGmt == null ? null : dateModifiedGmt.toIso8601String(),
        "src": src == null ? null : src,
        "name": name == null ? null : name,
        "alt": alt == null ? null : alt,
      };
}

class ProductVariant {
  int id;
  DateTime dateCreated;
  DateTime dateCreatedGmt;
  DateTime dateModified;
  DateTime dateModifiedGmt;
  String description;
  String permalink;
  String sku;
  String price;
  String regularPrice;
  String salePrice;
  dynamic dateOnSaleFrom;
  dynamic dateOnSaleFromGmt;
  dynamic dateOnSaleTo;
  dynamic dateOnSaleToGmt;
  bool onSale;
  String status;
  bool purchasable;
  bool virtual;
  bool downloadable;
  List<dynamic> downloads;
  int downloadLimit;
  int downloadExpiry;
  String taxStatus;
  String taxClass;
  bool manageStock;
  dynamic stockQuantity;
  String stockStatus;
  String backorders;
  bool backordersAllowed;
  bool backordered;
  String weight;
  Dimensions dimensions;
  String shippingClass;
  int shippingClassId;
  Image image;
  List<Attribute> attributes;

  ProductVariant({
    this.id,
    this.dateCreated,
    this.dateCreatedGmt,
    this.dateModified,
    this.dateModifiedGmt,
    this.description,
    this.permalink,
    this.sku,
    this.price,
    this.regularPrice,
    this.salePrice,
    this.dateOnSaleFrom,
    this.dateOnSaleFromGmt,
    this.dateOnSaleTo,
    this.dateOnSaleToGmt,
    this.onSale,
    this.status,
    this.purchasable,
    this.virtual,
    this.downloadable,
    this.downloads,
    this.downloadLimit,
    this.downloadExpiry,
    this.taxStatus,
    this.taxClass,
    this.manageStock,
    this.stockQuantity,
    this.stockStatus,
    this.backorders,
    this.backordersAllowed,
    this.backordered,
    this.weight,
    this.dimensions,
    this.shippingClass,
    this.shippingClassId,
    this.image,
    this.attributes,
  });

  ProductVariant copyWith({
    int id,
    DateTime dateCreated,
    DateTime dateCreatedGmt,
    DateTime dateModified,
    DateTime dateModifiedGmt,
    String description,
    String permalink,
    String sku,
    String price,
    String regularPrice,
    String salePrice,
    dynamic dateOnSaleFrom,
    dynamic dateOnSaleFromGmt,
    dynamic dateOnSaleTo,
    dynamic dateOnSaleToGmt,
    bool onSale,
    String status,
    bool purchasable,
    bool virtual,
    bool downloadable,
    List<dynamic> downloads,
    int downloadLimit,
    int downloadExpiry,
    String taxStatus,
    String taxClass,
    bool manageStock,
    dynamic stockQuantity,
    String stockStatus,
    String backorders,
    bool backordersAllowed,
    bool backordered,
    String weight,
    Dimensions dimensions,
    String shippingClass,
    int shippingClassId,
    Image image,
    List<Attribute> attributes,
  }) =>
      ProductVariant(
        id: id ?? this.id,
        dateCreated: dateCreated ?? this.dateCreated,
        dateCreatedGmt: dateCreatedGmt ?? this.dateCreatedGmt,
        dateModified: dateModified ?? this.dateModified,
        dateModifiedGmt: dateModifiedGmt ?? this.dateModifiedGmt,
        description: description ?? this.description,
        permalink: permalink ?? this.permalink,
        sku: sku ?? this.sku,
        price: price ?? this.price,
        regularPrice: regularPrice ?? this.regularPrice,
        salePrice: salePrice ?? this.salePrice,
        dateOnSaleFrom: dateOnSaleFrom ?? this.dateOnSaleFrom,
        dateOnSaleFromGmt: dateOnSaleFromGmt ?? this.dateOnSaleFromGmt,
        dateOnSaleTo: dateOnSaleTo ?? this.dateOnSaleTo,
        dateOnSaleToGmt: dateOnSaleToGmt ?? this.dateOnSaleToGmt,
        onSale: onSale ?? this.onSale,
        status: status ?? this.status,
        purchasable: purchasable ?? this.purchasable,
        virtual: virtual ?? this.virtual,
        downloadable: downloadable ?? this.downloadable,
        downloads: downloads ?? this.downloads,
        downloadLimit: downloadLimit ?? this.downloadLimit,
        downloadExpiry: downloadExpiry ?? this.downloadExpiry,
        taxStatus: taxStatus ?? this.taxStatus,
        taxClass: taxClass ?? this.taxClass,
        manageStock: manageStock ?? this.manageStock,
        stockQuantity: stockQuantity ?? this.stockQuantity,
        stockStatus: stockStatus ?? this.stockStatus,
        backorders: backorders ?? this.backorders,
        backordersAllowed: backordersAllowed ?? this.backordersAllowed,
        backordered: backordered ?? this.backordered,
        weight: weight ?? this.weight,
        dimensions: dimensions ?? this.dimensions,
        shippingClass: shippingClass ?? this.shippingClass,
        shippingClassId: shippingClassId ?? this.shippingClassId,
        image: image ?? this.image,
        attributes: attributes ?? this.attributes,
      );

  factory ProductVariant.fromRawJson(String str) =>
      ProductVariant.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory ProductVariant.fromJson(Map<String, dynamic> json) => ProductVariant(
        id: json["id"] == null ? null : json["id"],
        dateCreated: json["date_created"] == null
            ? null
            : DateTime.parse(json["date_created"]),
        dateCreatedGmt: json["date_created_gmt"] == null
            ? null
            : DateTime.parse(json["date_created_gmt"]),
        dateModified: json["date_modified"] == null
            ? null
            : DateTime.parse(json["date_modified"]),
        dateModifiedGmt: json["date_modified_gmt"] == null
            ? null
            : DateTime.parse(json["date_modified_gmt"]),
        description: json["description"] == null ? null : json["description"],
        permalink: json["permalink"] == null ? null : json["permalink"],
        sku: json["sku"] == null ? null : json["sku"],
        price: json["price"] == null ? null : json["price"],
        regularPrice:
            json["regular_price"] == null ? null : json["regular_price"],
        salePrice: json["sale_price"] == null ? null : json["sale_price"],
        dateOnSaleFrom: json["date_on_sale_from"],
        dateOnSaleFromGmt: json["date_on_sale_from_gmt"],
        dateOnSaleTo: json["date_on_sale_to"],
        dateOnSaleToGmt: json["date_on_sale_to_gmt"],
        onSale: json["on_sale"] == null ? null : json["on_sale"],
        status: json["status"] == null ? null : json["status"],
        purchasable: json["purchasable"] == null ? null : json["purchasable"],
        virtual: json["virtual"] == null ? null : json["virtual"],
        downloadable:
            json["downloadable"] == null ? null : json["downloadable"],
        downloads: json["downloads"] == null
            ? null
            : List<dynamic>.from(json["downloads"].map((x) => x)),
        downloadLimit:
            json["download_limit"] == null ? null : json["download_limit"],
        downloadExpiry:
            json["download_expiry"] == null ? null : json["download_expiry"],
        taxStatus: json["tax_status"] == null ? null : json["tax_status"],
        taxClass: json["tax_class"] == null ? null : json["tax_class"],
        manageStock: json["manage_stock"] == null ? null : json["manage_stock"],
        stockQuantity: json["stock_quantity"],
        stockStatus: json["stock_status"] == null ? null : json["stock_status"],
        backorders: json["backorders"] == null ? null : json["backorders"],
        backordersAllowed: json["backorders_allowed"] == null
            ? null
            : json["backorders_allowed"],
        backordered: json["backordered"] == null ? null : json["backordered"],
        weight: json["weight"] == null ? null : json["weight"],
        dimensions: json["dimensions"] == null
            ? null
            : Dimensions.fromJson(json["dimensions"]),
        shippingClass:
            json["shipping_class"] == null ? null : json["shipping_class"],
        shippingClassId: json["shipping_class_id"] == null
            ? null
            : json["shipping_class_id"],
        image: json["image"] == null ? null : Image.fromJson(json["image"]),
        attributes: json["attributes"] == null
            ? null
            : List<Attribute>.from(
                json["attributes"].map((x) => Attribute.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "date_created":
            dateCreated == null ? null : dateCreated.toIso8601String(),
        "date_created_gmt":
            dateCreatedGmt == null ? null : dateCreatedGmt.toIso8601String(),
        "date_modified":
            dateModified == null ? null : dateModified.toIso8601String(),
        "date_modified_gmt":
            dateModifiedGmt == null ? null : dateModifiedGmt.toIso8601String(),
        "description": description == null ? null : description,
        "permalink": permalink == null ? null : permalink,
        "sku": sku == null ? null : sku,
        "price": price == null ? null : price,
        "regular_price": regularPrice == null ? null : regularPrice,
        "sale_price": salePrice == null ? null : salePrice,
        "date_on_sale_from": dateOnSaleFrom,
        "date_on_sale_from_gmt": dateOnSaleFromGmt,
        "date_on_sale_to": dateOnSaleTo,
        "date_on_sale_to_gmt": dateOnSaleToGmt,
        "on_sale": onSale == null ? null : onSale,
        "status": status == null ? null : status,
        "purchasable": purchasable == null ? null : purchasable,
        "virtual": virtual == null ? null : virtual,
        "downloadable": downloadable == null ? null : downloadable,
        "downloads": downloads == null
            ? null
            : List<dynamic>.from(downloads.map((x) => x)),
        "download_limit": downloadLimit == null ? null : downloadLimit,
        "download_expiry": downloadExpiry == null ? null : downloadExpiry,
        "tax_status": taxStatus == null ? null : taxStatus,
        "tax_class": taxClass == null ? null : taxClass,
        "manage_stock": manageStock == null ? null : manageStock,
        "stock_quantity": stockQuantity,
        "stock_status": stockStatus == null ? null : stockStatus,
        "backorders": backorders == null ? null : backorders,
        "backorders_allowed":
            backordersAllowed == null ? null : backordersAllowed,
        "backordered": backordered == null ? null : backordered,
        "weight": weight == null ? null : weight,
        "dimensions": dimensions == null ? null : dimensions.toJson(),
        "shipping_class": shippingClass == null ? null : shippingClass,
        "shipping_class_id": shippingClassId == null ? null : shippingClassId,
        "image": image == null ? null : image.toJson(),
        "attributes": attributes == null
            ? null
            : List<dynamic>.from(attributes.map((x) => x.toJson())),
      };
}

class User {
  String id;
  String userLogin;
  String userPass;
  String userNicename;
  String userEmail;
  String userUrl;
  DateTime userRegistered;
  String userActivationKey;
  String userStatus;
  String displayName;

  User({
    this.id,
    this.userLogin,
    this.userPass,
    this.userNicename,
    this.userEmail,
    this.userUrl,
    this.userRegistered,
    this.userActivationKey,
    this.userStatus,
    this.displayName,
  });

  User copyWith({
    String id,
    String userLogin,
    String userPass,
    String userNicename,
    String userEmail,
    String userUrl,
    DateTime userRegistered,
    String userActivationKey,
    String userStatus,
    String displayName,
  }) =>
      User(
        id: id ?? this.id,
        userLogin: userLogin ?? this.userLogin,
        userPass: userPass ?? this.userPass,
        userNicename: userNicename ?? this.userNicename,
        userEmail: userEmail ?? this.userEmail,
        userUrl: userUrl ?? this.userUrl,
        userRegistered: userRegistered ?? this.userRegistered,
        userActivationKey: userActivationKey ?? this.userActivationKey,
        userStatus: userStatus ?? this.userStatus,
        displayName: displayName ?? this.displayName,
      );

  factory User.fromRawJson(String str) => User.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory User.fromJson(Map<String, dynamic> json) => User(
        id: json["ID"] == null ? null : json["ID"],
        userLogin: json["user_login"] == null ? null : json["user_login"],
        userPass: json["user_pass"] == null ? null : json["user_pass"],
        userNicename:
            json["user_nicename"] == null ? null : json["user_nicename"],
        userEmail: json["user_email"] == null ? null : json["user_email"],
        userUrl: json["user_url"] == null ? null : json["user_url"],
        userRegistered: json["user_registered"] == null
            ? null
            : DateTime.parse(json["user_registered"]),
        userActivationKey: json["user_activation_key"] == null
            ? null
            : json["user_activation_key"],
        userStatus: json["user_status"] == null ? null : json["user_status"],
        displayName: json["display_name"] == null ? null : json["display_name"],
      );

  Map<String, dynamic> toJson() => {
        "ID": id == null ? null : id,
        "user_login": userLogin == null ? null : userLogin,
        "user_pass": userPass == null ? null : userPass,
        "user_nicename": userNicename == null ? null : userNicename,
        "user_email": userEmail == null ? null : userEmail,
        "user_url": userUrl == null ? null : userUrl,
        "user_registered":
            userRegistered == null ? null : userRegistered.toIso8601String(),
        "user_activation_key":
            userActivationKey == null ? null : userActivationKey,
        "user_status": userStatus == null ? null : userStatus,
        "display_name": displayName == null ? null : displayName,
      };

  Customer toCustomer() => Customer(
      id: int.parse(id), dateCreated: userRegistered, firstName: displayName);
}

class Customer {
  int id;
  DateTime dateCreated;
  DateTime dateCreatedGmt;
  DateTime dateModified;
  DateTime dateModifiedGmt;
  String email;
  String firstName;
  String lastName;
  String role;
  String username;
  ShippingAddress billing;
  ShippingAddress shipping;
  bool isPayingCustomer;
  String avatarUrl;

  Customer({
    this.id,
    this.dateCreated,
    this.dateCreatedGmt,
    this.dateModified,
    this.dateModifiedGmt,
    this.email,
    this.firstName,
    this.lastName,
    this.role,
    this.username,
    this.billing,
    this.shipping,
    this.isPayingCustomer,
    this.avatarUrl,
  });

  Customer copyWith({
    int id,
    DateTime dateCreated,
    DateTime dateCreatedGmt,
    DateTime dateModified,
    DateTime dateModifiedGmt,
    String email,
    String firstName,
    String lastName,
    String role,
    String username,
    bool isPayingCustomer,
    String avatarUrl,
    List<dynamic> metaData,
  }) =>
      Customer(
        id: id ?? this.id,
        dateCreated: dateCreated ?? this.dateCreated,
        dateCreatedGmt: dateCreatedGmt ?? this.dateCreatedGmt,
        dateModified: dateModified ?? this.dateModified,
        dateModifiedGmt: dateModifiedGmt ?? this.dateModifiedGmt,
        email: email ?? this.email,
        firstName: firstName ?? this.firstName,
        lastName: lastName ?? this.lastName,
        role: role ?? this.role,
        username: username ?? this.username,
        billing: billing ?? this.billing,
        shipping: shipping ?? this.shipping,
        isPayingCustomer: isPayingCustomer ?? this.isPayingCustomer,
        avatarUrl: avatarUrl ?? this.avatarUrl,
      );

  factory Customer.fromRawJson(String str) =>
      Customer.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Customer.fromJson(Map<String, dynamic> json) => Customer(
        id: json["id"] == null ? null : json["id"],
        dateCreated: json["date_created"] == null
            ? null
            : DateTime.parse(json["date_created"]),
        dateCreatedGmt: json["date_created_gmt"] == null
            ? null
            : DateTime.parse(json["date_created_gmt"]),
        dateModified: json["date_modified"] == null
            ? null
            : DateTime.parse(json["date_modified"]),
        dateModifiedGmt: json["date_modified_gmt"] == null
            ? null
            : DateTime.parse(json["date_modified_gmt"]),
        email: json["email"] == null ? null : json["email"],
        firstName: json["first_name"] == null ? null : json["first_name"],
        lastName: json["last_name"] == null ? null : json["last_name"],
        role: json["role"] == null ? null : json["role"],
        username: json["username"] == null ? null : json["username"],
        billing: json["billing"] == null
            ? null
            : ShippingAddress.fromJson(json["billing"]),
        shipping: json["shipping"] == null
            ? null
            : ShippingAddress.fromJson(json["shipping"]),
        isPayingCustomer: json["is_paying_customer"] == null
            ? null
            : json["is_paying_customer"],
        avatarUrl: json["avatar_url"] == null ? null : json["avatar_url"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "date_created":
            dateCreated == null ? null : dateCreated.toIso8601String(),
        "date_created_gmt":
            dateCreatedGmt == null ? null : dateCreatedGmt.toIso8601String(),
        "date_modified":
            dateModified == null ? null : dateModified.toIso8601String(),
        "date_modified_gmt":
            dateModifiedGmt == null ? null : dateModifiedGmt.toIso8601String(),
        "email": email == null ? null : email,
        "first_name": firstName == null ? null : firstName,
        "last_name": lastName == null ? null : lastName,
        "role": role == null ? null : role,
        "username": username == null ? null : username,
        "billing": billing == null ? null : billing.toJson(),
        "shipping": shipping == null ? null : shipping.toJson(),
        "is_paying_customer":
            isPayingCustomer == null ? null : isPayingCustomer,
        "avatar_url": avatarUrl == null ? null : avatarUrl,
      };
}

class OrderNote {
  int id;
  String author;
  DateTime dateCreated;
  DateTime dateCreatedGmt;
  String note;
  bool customerNote;

  OrderNote({
    this.id,
    this.author,
    this.dateCreated,
    this.dateCreatedGmt,
    this.note,
    this.customerNote,
  });

  OrderNote copyWith({
    int id,
    String author,
    DateTime dateCreated,
    DateTime dateCreatedGmt,
    String note,
    bool customerNote,
  }) =>
      OrderNote(
        id: id ?? this.id,
        author: author ?? this.author,
        dateCreated: dateCreated ?? this.dateCreated,
        dateCreatedGmt: dateCreatedGmt ?? this.dateCreatedGmt,
        note: note ?? this.note,
        customerNote: customerNote ?? this.customerNote,
      );

  factory OrderNote.fromRawJson(String str) =>
      OrderNote.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory OrderNote.fromJson(Map<String, dynamic> json) => OrderNote(
        id: json["id"] == null ? null : json["id"],
        author: json["author"] == null ? null : json["author"],
        dateCreated: json["date_created"] == null
            ? null
            : DateTime.parse(json["date_created"]),
        dateCreatedGmt: json["date_created_gmt"] == null
            ? null
            : DateTime.parse(json["date_created_gmt"]),
        note: json["note"] == null ? null : json["note"],
        customerNote:
            json["customer_note"] == null ? null : json["customer_note"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "author": author == null ? null : author,
        "date_created":
            dateCreated == null ? null : dateCreated.toIso8601String(),
        "date_created_gmt":
            dateCreatedGmt == null ? null : dateCreatedGmt.toIso8601String(),
        "note": note == null ? null : note,
        "customer_note": customerNote == null ? null : customerNote,
      };
}

class ProductCategory {
  int id;
  String name;
  String slug;
  int parent;
  String description;
  String display;
  List<dynamic> image;
  int menuOrder;
  int count;

  ProductCategory({
    this.id,
    this.name,
    this.slug,
    this.parent,
    this.description,
    this.display,
    this.image,
    this.menuOrder,
    this.count,
  });

  ProductCategory copyWith({
    int id,
    String name,
    String slug,
    int parent,
    String description,
    String display,
    List<dynamic> image,
    int menuOrder,
    int count,
  }) =>
      ProductCategory(
        id: id ?? this.id,
        name: name ?? this.name,
        slug: slug ?? this.slug,
        parent: parent ?? this.parent,
        description: description ?? this.description,
        display: display ?? this.display,
        image: image ?? this.image,
        menuOrder: menuOrder ?? this.menuOrder,
        count: count ?? this.count,
      );

  factory ProductCategory.fromRawJson(String str) =>
      ProductCategory.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory ProductCategory.fromJson(Map<String, dynamic> json) =>
      ProductCategory(
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? null : json["name"],
        slug: json["slug"] == null ? null : json["slug"],
        parent: json["parent"] == null ? null : json["parent"],
        description: json["description"] == null ? null : json["description"],
        display: json["display"] == null ? null : json["display"],
        image: json["image"] == null
            ? null
            : List<dynamic>.from(json["image"].map((x) => x)),
        menuOrder: json["menu_order"] == null ? null : json["menu_order"],
        count: json["count"] == null ? null : json["count"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "name": name == null ? null : name,
        "slug": slug == null ? null : slug,
        "parent": parent == null ? null : parent,
        "description": description == null ? null : description,
        "display": display == null ? null : display,
        "image": image == null ? null : List<dynamic>.from(image.map((x) => x)),
        "menu_order": menuOrder == null ? null : menuOrder,
        "count": count == null ? null : count,
      };
}

class PaymentMethod {
  String id;
  String title;
  String description;
  int order;
  bool enabled;
  String methodTitle;
  String methodDescription;

  PaymentMethod({
    this.id,
    this.title,
    this.description,
    this.order,
    this.enabled,
    this.methodTitle,
    this.methodDescription,
  });

  PaymentMethod copyWith({
    String id,
    String title,
    String description,
    int order,
    bool enabled,
    String methodTitle,
    String methodDescription,
  }) =>
      PaymentMethod(
        id: id ?? this.id,
        title: title ?? this.title,
        description: description ?? this.description,
        order: order ?? this.order,
        enabled: enabled ?? this.enabled,
        methodTitle: methodTitle ?? this.methodTitle,
        methodDescription: methodDescription ?? this.methodDescription,
      );

  factory PaymentMethod.fromRawJson(String str) =>
      PaymentMethod.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory PaymentMethod.fromJson(Map<String, dynamic> json) => PaymentMethod(
        id: json["id"] == null ? null : json["id"],
        title: json["title"] == null ? null : json["title"],
        description: json["description"] == null ? null : json["description"],
        order: json["order"] == null
            ? null
            : (json["order"] is String)
                ? int.tryParse(json["order"], radix: 0)
                : json["order"],
        enabled: json["enabled"] == null ? null : json["enabled"],
        methodTitle: json["method_title"] == null ? null : json["method_title"],
        methodDescription: json["method_description"] == null
            ? null
            : json["method_description"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "title": title == null ? null : title,
        "description": description == null ? null : description,
        "order": order == null ? null : order,
        "enabled": enabled == null ? null : enabled,
        "method_title": methodTitle == null ? null : methodTitle,
        "method_description":
            methodDescription == null ? null : methodDescription,
      };
}

class Metadata {
  final int id;
  final String key;
  final String value;

  Metadata({
    this.id,
    this.key,
    this.value,
  });

  Metadata copyWith({
    int id,
    String key,
    String value,
  }) =>
      Metadata(
        id: id ?? this.id,
        key: key ?? this.key,
        value: value ?? this.value,
      );

  factory Metadata.fromJson(String str) => Metadata.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory Metadata.fromMap(Map<String, dynamic> json) => Metadata(
        id: json["id"],
        key: json["key"],
        value: json["value"],
      );

  Map<String, dynamic> toMap() => {
        if (id != null) "id": id,
        "key": key,
        "value": value,
      };
}

class Config {
  static final String checkOutMetaKey = "checkout_status";
  static final String currency = "MYR";
}
