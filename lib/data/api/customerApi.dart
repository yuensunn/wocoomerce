import 'package:dio/dio.dart';
import '../model.dart';

class CustomerApi {
  static Dio dio = new Dio(BaseOptions(
      connectTimeout: 5000,
      baseUrl: "https://store.emingli360.com/wp-json/wc/v3/"));
  static Dio customDio = new Dio(BaseOptions(
      connectTimeout: 5000,
      baseUrl: "https://store.emingli360.com/wp-json/custom/v2/"));

  static Future<Customer> getCustomerBySub(String sub) async {
    try {
      final response = await customDio.get<List<dynamic>>("/getCustomerBySub",
          queryParameters: {"sub": sub});

      if (response.data.length <= 0) return null;
      return response.data.map((x) => User.fromJson(x))?.first?.toCustomer();
    } on DioError catch (e) {
      throw Failure(e.message);
    } catch (e) {
      throw Failure(e.toString());
    }
  }
}
