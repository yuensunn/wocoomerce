import 'package:dio/dio.dart';
import '../model.dart';

class OrderApi {
  static Dio dio = new Dio(BaseOptions(
      connectTimeout: 5000,
      baseUrl: "https://store.emingli360.com/wp-json/wc/v3/"));

  static Future<Order> getOrder(int orderId) async {
    try {
      final response = await dio.get<Map<String, dynamic>>("/orders/$orderId");

      return Order.fromJson(response.data);
    } on DioError catch (e) {
      throw Failure(e.message);
    } catch (e) {
      throw Failure(e.toString());
    }
  }

  // Woocommerce's requirement
  // Email must be present & with valid format
  // Varition Id should not be passed if theres none
  static Future<Order> createOrder(Order order) async {
    try {
      final response = await dio.post("/orders", data: order.toJson());
      return Order.fromJson(response.data);
    } on DioError catch (e) {
      throw Failure(e.message);
    } catch (e) {
      throw Failure(e.toString());
    }
  }

  // Woocommerce's requirement
  // update order with products will just add into the existing ones

  static Future<Order> updateOrder(Order order) async {
    var test = order.toRawJson();
    print(test);
    try {
      final response =
          await dio.put("/orders/${order.id}", data: order.toJson());
      return Order.fromJson(response.data);
    } on DioError catch (e) {
      throw Failure(e.message);
    } catch (e) {
      throw Failure(e.toString());
    }
  }

  static Future<List<Order>> listOrders(
      {int page = 1,
      int perPage = 20,
      String search,
      List<int> include,
      List<int> exclude,
      int customer,
      List<OrderStatus> status = const [OrderStatus.any]}) async {
    try {
      final response =
          await dio.get<List<dynamic>>("/orders", queryParameters: {
        "page": page,
        "per_page": perPage,
        "search": search,
        "include": include,
        "exclude": exclude,
        if (customer != null) "customer": customer,
        "status": status.map((x) => x.value).toList()
      });
      return response.data.map((x) => Order.fromJson(x)).toList();
    } on DioError catch (e) {
      throw Failure(e.message);
    } catch (e) {
      throw Failure(e.toString());
    }
  }
}
