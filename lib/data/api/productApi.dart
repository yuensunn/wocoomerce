import 'package:dio/dio.dart';
import '../model.dart';

class ProductApi {
  static Dio dio = new Dio(BaseOptions(
      connectTimeout: 5000,
      baseUrl: "https://store.emingli360.com/wp-json/wc/v3/"));

  static Future<Product> getProduct(int productId) async {
    try {
      final response =
          await dio.get<Map<String, dynamic>>("/products/$productId");
      return Product.fromJson(response.data);
    } on DioError catch (e) {
      throw Failure(e.message);
    } catch (e) {
      throw Failure(e.toString());
    }
  }

  static Future<List<Product>> listProducts(
      {int page = 1,
      int perPage = 20,
      String search,
      List<int> include,
      List<int> exclude,
      bool onSale,
      bool featured,
      String status = "any"}) async {
    try {
      final response =
          await dio.get<List<dynamic>>("/products", queryParameters: {
        "page": page,
        "per_page": perPage,
        "search": search,
        "include": include,
        "exclude": exclude,
        "status": status,
        if (onSale != null) "on_sale": onSale,
        if (featured != null) "featured": featured
      });
      return response.data.map((x) => Product.fromJson(x)).toList();
    } on DioError catch (e) {
      throw Failure(e.message);
    } catch (e) {
      throw Failure(e.toString());
    }
  }

  static Future<List<ProductVariant>> listVariants(int productId,
      {int page = 1,
      int perPage = 20,
      int variantId = -1,
      String search,
      List<int> include,
      List<int> exclude,
      String status = "any"}) async {
    try {
      final response = await dio.get<List<dynamic>>(
          "/products/$productId/variations/${variantId >= 0 ? variantId : ""}",
          queryParameters: {
            "page": page,
            "per_page": perPage,
            "search": search,
            "include": include,
            "exclude": exclude,
            "status": status
          });
      return response.data.map((x) => ProductVariant.fromJson(x)).toList();
    } on DioError catch (e) {
      throw Failure(e.message);
    } catch (e) {
      throw Failure(e.toString());
    }
  }

  static Future<List<Attribute>> listAttributes({int attributeId = -1}) async {
    try {
      final response = await dio.get<List<dynamic>>(
          "/products/attributes${attributeId >= 0 ? '/$attributeId' : ""}");
      return response.data.map((x) => Attribute.fromJson(x)).toList();
    } on DioError catch (e) {
      throw Failure(e.message);
    } catch (e) {
      throw Failure(e.toString());
    }
  }

  static Future<List<Category>> listCategories(
      {int page = 1,
      int perPage = 20,
      String search,
      List<int> include,
      List<int> exclude,
      String status = "any"}) async {
    try {
      final response = await dio.get<List<dynamic>>("/products/categories");
      return response.data.map((x) => Category.fromJson(x)).toList();
    } on DioError catch (e) {
      throw Failure(e.message);
    } catch (e) {
      throw Failure(e.toString());
    }
  }
}
