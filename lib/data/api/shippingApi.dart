import 'package:dio/dio.dart';
import '../model.dart';

class ShippingApi {
  static Dio dio = new Dio(BaseOptions(
      connectTimeout: 5000,
      baseUrl: "https://store.emingli360.com/wp-json/wc/v3/"));
  static Future<List<ShippingZoneMethod>> listShippingZoneMethods() async {
    try {
      final response =
          await dio.get<List<dynamic>>("/shipping/zones/2/methods");
      return response.data.map((x) => ShippingZoneMethod.fromJson(x)).toList();
    } on DioError catch (e) {
      throw Failure(e.message);
    } catch (e) {
      throw Failure(e.toString());
    }
  }

  static Future<List<ShippingMethod>> listShippingMethods() async {
    try {
      final response = await dio.get<List<dynamic>>("/shipping_methods");
      return response.data.map((x) => ShippingMethod.fromJson(x)).toList();
    } on DioError catch (e) {
      throw Failure(e.message);
    } catch (e) {
      throw Failure(e.toString());
    }
  }
}
