import 'package:dio/dio.dart';
import '../model.dart';

class PaymentApi {
  static Dio dio = new Dio(BaseOptions(
      connectTimeout: 5000,
      baseUrl: "https://store.emingli360.com/wp-json/wc/v3/"));

  static Future<List<PaymentMethod>> listPayments() async {
    try {
      final response = await dio.get<List<dynamic>>("/payment_gateways");
      return response.data.map((x) => PaymentMethod.fromJson(x)).toList();
    } on DioError catch (e) {
      throw Failure(e.message);
    } catch (e) {
      throw Failure(e.toString());
    }
  }
}
