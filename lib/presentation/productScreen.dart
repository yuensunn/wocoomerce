import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';
import 'package:marketplace/data/data.dart' hide Image;
import 'package:provider/provider.dart';
import 'package:marketplace/domain/domain.dart';
import 'package:marketplace/presentation/presentation.dart';

class ProductScreen extends StatelessWidget {
  const ProductScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ProductsNotifier pn = Provider.of<ProductsNotifier>(context);
    CategoriesNotifier cn = Provider.of<CategoriesNotifier>(context);
    List<Product> products = pn.products;
    return Scaffold(
        appBar: AppBar(
          title: Text("Products"),
          actions: <Widget>[
            FlatButton(
              child: Text("My Orders"),
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => MyOrdersScreen()));
              },
            )
          ],
        ),
        body: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 100,
                child: ListView.separated(
                  shrinkWrap: true,
                  itemCount: cn.categories.length,
                  itemBuilder: (context, index) {
                    return Card(
                      child: GridTile(
                        child: Image.network(cn.categories[index].image.src),
                      ),
                    );
                  },
                  separatorBuilder: (context, index) {
                    return Container(width: 10);
                  },
                  scrollDirection: Axis.horizontal,
                ),
              ),
              GridView.builder(
                primary: false,
                shrinkWrap: true,
                itemCount: products.length,
                gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
                    childAspectRatio: 0.6, crossAxisCount: 2),
                itemBuilder: (BuildContext context, int index) {
                  return Card(
                    child: FlatButton(
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ProductDetailScreen(
                                    Left(products[index].id))));
                      },
                      child: GridTile(
                        header: Text(products[index].name),
                        footer: Text(products[index].regularPrice),
                        child: Image.network(products[index].images[0]?.src ??
                            ""), //just for testing, will fill with image later
                      ),
                    ),
                  );
                },
              )
            ],
          ),
        ));
  }
}
