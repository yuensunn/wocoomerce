import 'package:dartz/dartz.dart' hide State;
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:marketplace/data/data.dart' hide Image;
import 'package:marketplace/presentation/presentation.dart';
import 'package:provider/provider.dart';
import 'package:marketplace/domain/service/service.dart';

class ProductDetailScreen extends StatefulWidget {
  final Either<int, Product> product;
  ProductDetailScreen(this.product, {Key key}) : super(key: key);
  _ProductDetailScreenState createState() => _ProductDetailScreenState();
}

class _ProductDetailScreenState extends State<ProductDetailScreen> {
  int variantId;
  @override
  Widget build(BuildContext context) {
    CartNotifier cn = Provider.of<CartNotifier>(context);

    return ChangeNotifierProvider(
        create: (context) => ProductNotifier(defaultProduct: widget.product),
        child: Consumer<ProductNotifier>(
          builder: (context, productNotifier, _) {
            Product product = productNotifier.product;
            List<ProductVariant> variants = productNotifier.productVariants;

            if (productNotifier.isLoading)
              return Material(
                  child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[CircularProgressIndicator()],
              ));
            return Scaffold(
              appBar: AppBar(title: Text(product.name), actions: <Widget>[
                FlatButton(
                  color: Colors.orange,
                  child: Text(cn.itemCount.toString()),
                  onPressed: () {
                    Navigator.of(context).push(
                        MaterialPageRoute(builder: (context) => CartScreen()));
                  },
                ),
              ]),
              body: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Image.network(product.images[0]?.src ?? ""),
                    Html(data: product.description),
                    Column(
                      children: <Widget>[
                        Text("Variant"),
                        Wrap(
                          direction: Axis.horizontal,
                          children: variants
                              .map((v) => SizedBox(
                                    width: 100,
                                    height: 100,
                                    child: FlatButton(
                                      padding: v.id == variantId
                                          ? EdgeInsets.all(10)
                                          : EdgeInsets.zero,
                                      onPressed: () {
                                        setState(() {
                                          variantId = v.id;
                                        });
                                      },
                                      child: GridTile(
                                        header: Text(v.attributes
                                            .map((x) => "${x.name} ${x.option}")
                                            .join("\n")),
                                        child: Image.network(v.image.src),
                                      ),
                                    ),
                                  ))
                              .toList(),
                        )
                      ],
                    ),
                    FlatButton(
                        color: Colors.green,
                        child: Text("Add to Bag"),
                        onPressed: () {
                          ProductVariant pv;
                          if (product.type == ProductType.variable) {
                            pv = variants.firstWhere((x) => x.id == variantId);
                          }
                          cn.addLineItem(product, variant: pv);
                        })
                  ],
                ),
              ),
            );
          },
        ));
  }
}
