import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:marketplace/data/data.dart' hide Image;
import 'package:marketplace/domain/domain.dart';
import 'package:provider/provider.dart';

class CheckOutScreen extends StatefulWidget {
  final Order order;
  final void Function() onFailed;
  CheckOutScreen(this.order, {Key key, this.onFailed}) : super(key: key);

  @override
  _CheckOutScreenState createState() => _CheckOutScreenState();
}

class _CheckOutScreenState extends State<CheckOutScreen> {
  CheckoutNotifier checkoutNotifier;
  int steps = 0;
  @override
  void initState() {
    super.initState();
    checkoutNotifier = CheckoutNotifier(
      order: widget.order.copyWith(
        shippingLines: widget.order.shippingLines ?? List<ShippingLine>(),
        shipping: widget.order.shipping ?? ShippingAddress(),
        billing: widget.order.billing ?? ShippingAddress(),
      ),
    );

    Future.delayed(Duration.zero).then((_) {
      checkoutNotifier
          .setCustomer(Provider.of<CustomerNotifier>(context).customer);
      // if have ID means its an existing order, skip creating
      if (checkoutNotifier.order.id == null)
        checkoutNotifier.createOrder().then((_) {
          print(checkoutNotifier.order);
        });
    });
  }

  Widget _buildTextField({
    String intialValue = "",
    String label = "",
    void Function(String) onChanged,
  }) {
    return TextFormField(
      onChanged: onChanged,
      initialValue: intialValue,
      decoration: InputDecoration(labelText: label),
    );
  }

  Widget _buildForm(
      ShippingAddress shippingAddress,
      void Function({
    String firstName,
    String lastName,
    String address1,
    String address2,
    String city,
    String state,
    String postcode,
    String country,
    String email,
    String phone,
  })
          onEdit) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        _buildTextField(
          intialValue: shippingAddress.firstName,
          onChanged: (val) {
            onEdit(firstName: val);
          },
          label: "First Name",
        ),
        _buildTextField(
          intialValue: shippingAddress.lastName,
          onChanged: (val) {
            onEdit(lastName: val);
          },
          label: "Last Name",
        ),
        _buildTextField(
          intialValue: shippingAddress.email,
          onChanged: (val) {
            onEdit(email: val);
          },
          label: "Email",
        ),
        _buildTextField(
          intialValue: shippingAddress.city,
          onChanged: (val) {
            onEdit(city: val);
          },
          label: "City",
        ),
        _buildTextField(
          intialValue: shippingAddress.state,
          onChanged: (val) {
            onEdit(state: val);
          },
          label: "State",
        ),
        _buildTextField(
          intialValue: shippingAddress.country,
          onChanged: (val) {
            onEdit(country: val);
          },
          label: "Country",
        ),
        _buildTextField(
          intialValue: shippingAddress.postcode,
          onChanged: (val) {
            onEdit(postcode: val);
          },
          label: "Post Code",
        ),
        _buildTextField(
          intialValue: shippingAddress.phone,
          label: "Phone",
          onChanged: (val) {
            onEdit(phone: val);
          },
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => checkoutNotifier,
      child: Consumer<CheckoutNotifier>(builder: (context, lo, _) {
        List<PaymentMethod> paymentMethods =
            Provider.of<PaymentNotifier>(context).paymentMethods;
        List<ShippingMethod> shippingMethod =
            Provider.of<ShippingNotifier>(context).shippingMethods;
        return Stack(children: <Widget>[
          Scaffold(
            appBar: AppBar(),
            body: Stepper(
              currentStep: steps,
              onStepContinue: () {
                print(checkoutNotifier);
                if (steps < 2) {
                  checkoutNotifier.updateOrder(
                      metaData: Metadata(
                          key: Config.checkOutMetaKey,
                          value: steps == 0 ? "address" : "shipping"));
                  setState(() {
                    steps++;
                  });
                }
              },
              onStepCancel: () {
                if (steps > 0)
                  setState(() {
                    steps--;
                  });
              },
              type: StepperType.horizontal,
              steps: [
                Step(
                    title: Text("Address"),
                    content: Form(
                      child: Column(
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Text("Ship to billing address"),
                              Checkbox(
                                value: lo.isShippingBilling,
                                onChanged: (_) {
                                  lo.setIsShippingBilling(_);
                                },
                              )
                            ],
                          ),
                          Text("Billing"),
                          _buildForm(lo.order.billing, lo.setBillingAddress),
                          if (!lo.isShippingBilling) Text("Shipping"),
                          if (!lo.isShippingBilling)
                            _buildForm(
                                lo.order.shipping, lo.setShippingAddress),
                        ],
                      ),
                    )),
                Step(
                    title: Text("Shipping"),
                    content: ListView.separated(
                      primary: false,
                      shrinkWrap: true,
                      itemCount: shippingMethod.length,
                      itemBuilder: (context, index) {
                        return Card(
                            color: lo.getShippingLine.methodId ==
                                    shippingMethod[index].id
                                ? Colors.orange[100]
                                : Colors.white,
                            child: FlatButton(
                              onPressed: () {
                                lo.setShippingMethod(shippingMethod[index]);
                              },
                              child: ListTile(
                                title: Text(shippingMethod[index].title),
                                subtitle: Column(
                                  children: <Widget>[
                                    Html(
                                      data: shippingMethod[index].description,
                                    ),
                                  ],
                                ),
                              ),
                            ));
                      },
                      separatorBuilder: (context, index) {
                        return Container(height: 20);
                      },
                    )),
                Step(
                    title: Text("Payment"),
                    content: ListView.separated(
                      primary: false,
                      shrinkWrap: true,
                      itemCount: paymentMethods.length,
                      itemBuilder: (context, index) {
                        return Card(
                            color: paymentMethods[index].id ==
                                    lo.order.paymentMethod
                                ? Colors.orange[100]
                                : Colors.white,
                            child: FlatButton(
                              onPressed: () {
                                lo.setPaymentMethod(paymentMethods[index]);
                              },
                              child: ListTile(
                                title: Text(paymentMethods[index].title),
                                subtitle: Column(
                                  children: <Widget>[
                                    Html(
                                      data: paymentMethods[index].description,
                                    ),
                                    Html(
                                      data: paymentMethods[index]
                                          .methodDescription,
                                    )
                                  ],
                                ),
                              ),
                            ));
                      },
                      separatorBuilder: (context, index) {
                        return Container(height: 20);
                      },
                    ))
              ],
            ),
          ),
          if (checkoutNotifier.isLoading)
            Positioned(
              left: 0,
              right: 0,
              top: 0,
              bottom: 0,
              child: Container(
                color: Colors.black.withOpacity(0.4),
                child: Container(
                    alignment: Alignment.center,
                    child: CircularProgressIndicator(
                      backgroundColor: Colors.transparent,
                      valueColor:
                          const AlwaysStoppedAnimation<Color>(Colors.white38),
                    )),
              ),
            )
        ]);
      }),
    );
  }
}
