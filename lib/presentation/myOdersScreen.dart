import 'package:flutter/material.dart';
import 'package:marketplace/data/data.dart';
import 'package:marketplace/domain/domain.dart';
import 'package:marketplace/presentation/checkoutScreen.dart';
import 'package:provider/provider.dart';

class MyOrdersScreen extends StatefulWidget {
  MyOrdersScreen({Key key}) : super(key: key);

  @override
  _MyOrdersScreenState createState() => _MyOrdersScreenState();
}

class _MyOrdersScreenState extends State<MyOrdersScreen> {
  @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero).then((_) {
      int customerId = Provider.of<CustomerNotifier>(context).customer?.id;
      OrdersNotifier on = Provider.of<OrdersNotifier>(context);
      if (on.myOrders.length <= 0) {
        Provider.of<OrdersNotifier>(context)
            .getMyOrders(customerId, status: [OrderStatus.pending]);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    OrdersNotifier on = Provider.of<OrdersNotifier>(context);
    List<ExtendedOrder> extendedOrders = on.myOrders;
    return Scaffold(
      appBar: AppBar(),
      body: Builder(
        builder: (context) {
          if (on.isLoading) return CircularProgressIndicator();

          return ListView.separated(
            itemCount: extendedOrders.length,
            itemBuilder: (context, index) => Card(
              child: FlatButton(
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) =>
                              CheckOutScreen(extendedOrders[index].order)));
                },
                child: ListTile(
                  title: Text(extendedOrders[index].order.id.toString()),
                  subtitle: Column(
                    children: <Widget>[
                      Text(extendedOrders[index].order.status),
                      Text(extendedOrders[index].order.customerId.toString()),
                      ...extendedOrders[index].order.lineItems.map((x) =>
                          Text("${x.productId} ${x.quantity} ${x.variationId}"))
                    ],
                  ),
                ),
              ),
            ),
            separatorBuilder: (context, index) => Container(
              height: 10,
            ),
          );
        },
      ),
    );
  }
}
