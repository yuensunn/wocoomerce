import 'package:flutter/material.dart';
import 'package:marketplace/data/data.dart' hide Image;
import 'package:marketplace/domain/domain.dart';
import 'package:marketplace/presentation/presentation.dart';
import 'package:provider/provider.dart';

class CartScreen extends StatefulWidget {
  CartScreen({Key key}) : super(key: key);

  @override
  _CartScreenState createState() => _CartScreenState();
}

class _CartScreenState extends State<CartScreen> {
  Set<String> selectedIds = Set<String>();
  @override
  Widget build(BuildContext context) {
    CartNotifier cn = Provider.of<CartNotifier>(context);
    List<ExtendedLineItem> items = cn.lineItems.values.toList();
    return Scaffold(
      appBar: AppBar(
        title: Text("Cart"),
        actions: <Widget>[
          FlatButton(
            color: Colors.orange,
            child: Text("checkout"),
            onPressed: () {
              setState(() {
                if (selectedIds.length > 0) {
                  Order o = cn.createOrder(selectedIds);

                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => CheckOutScreen(o)));
                }
              });
            },
          ),
        ],
      ),
      body: ListView.separated(
        itemCount: items.length,
        itemBuilder: (context, index) {
          return ListTile(
            leading: Image.network(items[index]?.productVariant?.image?.src ??
                items[index]?.product?.images[0]?.src),
            title: Text(items[index].product.name),
            subtitle: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Text("x ${items[index].quantity}"),
                Text(items[index]
                        ?.productVariant
                        ?.attributes
                        ?.map((a) => "${a.name} ${a.option}")
                        ?.join(" ") ??
                    "")
              ],
            ),
            trailing: FlatButton(
              color: selectedIds.contains(items[index].id)
                  ? Colors.orange
                  : Colors.orange[200],
              child: Text(selectedIds.contains(items[index].id) ? "On" : "Off"),
              onPressed: () {
                setState(() {
                  if (selectedIds.contains(items[index].id)) {
                    selectedIds.remove(items[index].id);
                  } else {
                    selectedIds.add(items[index].id);
                  }
                });
              },
            ),
          );
        },
        separatorBuilder: (context, index) {
          return Container(height: 20);
        },
      ),
    );
  }
}
