import 'package:flutter/foundation.dart' hide Category;
import 'package:marketplace/data/data.dart';
import 'package:meta/meta.dart';
import 'package:dartz/dartz.dart' hide Order;

class ProductNotifier extends ChangeNotifier {
  bool isLoading;
  Product _product;
  Product get product => _product;
  List<ProductVariant> _productVariants;
  List<ProductVariant> get productVariants => _productVariants;
  ProductNotifier({@required Either<int, Product> defaultProduct}) {
    defaultProduct.fold((productId) async {
      _product = Product(id: productId);
      await getProduct(productId);
      await getProductVariants();
    }, (product) {
      _product = product;
      getProductVariants();
    });
  }

  Future getProductVariants() async {
    isLoading = true;
    notifyListeners();
    _productVariants = await ProductApi.listVariants(product.id);
    isLoading = false;
    notifyListeners();
  }

  Future getProduct(int productId) async {
    isLoading = true;
    notifyListeners();
    _product = await ProductApi.getProduct(productId);
    isLoading = false;
    notifyListeners();
  }
}
