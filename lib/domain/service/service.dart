export './customerService.dart';
export './productService.dart';
export './productsService.dart';
export './categoriesService.dart';

import 'package:flutter/foundation.dart' hide Category;
import 'package:marketplace/data/data.dart';
import 'package:marketplace/domain/model.dart';

class AddressNotifier extends ChangeNotifier {
  ShippingAddress _shippingAddress;
  ShippingAddress _billingAddress;
  bool isShippingBilling = true;
  ShippingAddress get shippingAddres => _shippingAddress;
  ShippingAddress get billingAddres =>
      isShippingBilling ? _shippingAddress : _billingAddress;

  void editShippingAddress({
    String firstName,
    String lastName,
    String address1,
    String address2,
    String city,
    String state,
    String postcode,
    String country,
    String email,
    String phone,
  }) {
    _shippingAddress = _shippingAddress.copyWith(
        firstName: firstName,
        lastName: lastName,
        address1: address2,
        city: city,
        state: state,
        postcode: postcode,
        country: country,
        email: email,
        phone: phone);
    notifyListeners();
  }

  void editBillingAddress({
    String firstName,
    String lastName,
    String address1,
    String address2,
    String city,
    String state,
    String postcode,
    String country,
    String email,
    String phone,
  }) {
    _billingAddress = _billingAddress.copyWith(
        firstName: firstName,
        lastName: lastName,
        address1: address2,
        city: city,
        state: state,
        postcode: postcode,
        country: country,
        email: email,
        phone: phone);
    notifyListeners();
  }
}

class CartNotifier extends ChangeNotifier {
  Map<String, ExtendedLineItem> lineItems = Map<String, ExtendedLineItem>();
  int get itemCount =>
      lineItems.values.fold(0, (prev, li) => li.quantity + prev);
  int get itemTypeCount => lineItems.keys.length;
  addLineItem(Product product, {ProductVariant variant, int quantity = 1}) {
    try {
      String key = "${product.id}${variant?.id}";
      if (lineItems.containsKey(key)) {
        lineItems.update(
            key,
            (prev) => ExtendedLineItem(product,
                variant: variant, q: prev.quantity + quantity));
      } else {
        lineItems.putIfAbsent(key,
            () => ExtendedLineItem(product, variant: variant, q: quantity));
      }
      notifyListeners();
    } catch (e) {
      print(e.toString());
    }
  }

  editLineItemQuantity(String id, int quantity) {
    try {
      if (lineItems.containsKey(id)) {
        lineItems.update(id, (prev) => prev.copyWith(quantity: quantity));
        notifyListeners();
      }
    } catch (e) {
      print(e.toString());
    }
  }

  Order createOrder(Set<String> extendedIds) {
    List<ExtendedLineItem> selected =
        extendedIds.map((x) => lineItems[x]).where((z) => z != null).toList();
    lineItems.removeWhere((k, _) => extendedIds.contains(k));
    notifyListeners();
    return Order(lineItems: selected);
  }

  // CAN ONLY LOAD FROM LOCAL ORDER!!!!!!!!!!!!!!!!!
  void loadFromLocalOrder(Order order) {
    try {
      lineItems.addEntries(order.lineItems
          .cast<ExtendedLineItem>()
          .map((x) => MapEntry<String, ExtendedLineItem>(x.id, x)));
      notifyListeners();
    } catch (e) {
      throw e;
    }
  }
}

class OrdersNotifier extends ChangeNotifier {
  bool isLoading;
  // Need extended order as the original order does not contain info like product image and title and etc
  List<ExtendedOrder> myOrders;
  OrdersNotifier({this.myOrders = const []});
  Future getMyOrders(int customerId,
      {List<OrderStatus> status = const []}) async {
    isLoading = true;
    notifyListeners();
    myOrders = (await OrderApi.listOrders(customer: customerId, status: status))
        .map((x) => ExtendedOrder(x))
        .toList();
    isLoading = false;
    notifyListeners();
  }
}

class CheckoutNotifier extends ChangeNotifier {
  bool isLoading = false;
  Order order;
  bool isShippingBilling = true;
  // Handle the logic when same shipping & billing address happens
  ShippingAddress get getBilling =>
      isShippingBilling ? order.billing : order.shipping;

  // Current situation only allow a single shipping line, hence this quick util function to retrieve the single shipping line from the list
  ShippingLine get getShippingLine =>
      (order?.shippingLines != null && order.shippingLines.length > 0)
          ? order.shippingLines.first
          : ShippingLine();

  CheckoutNotifier({
    this.order,
    this.isShippingBilling = true,
  });

  void setIsShippingBilling(bool val) {
    isShippingBilling = val;
    notifyListeners();
  }

  void setCustomer(Customer customer) {
    if (customer != null) {
      order.customerId = customer.id;
      notifyListeners();
    }
  }

  // The order might be local created order or orders loaded from the internet
  void setOrder(Order o) {
    order = o;
    order.shipping = order.shipping ?? ShippingAddress();
    order.billing = order.billing ?? ShippingAddress();
    notifyListeners();
  }

  // reset current order
  void clearOrder() {
    order = Order();
    order.shipping = order.shipping ?? ShippingAddress();
    order.billing = order.billing ?? ShippingAddress();
    notifyListeners();
  }

  void setPaymentMethod(PaymentMethod method) {
    order.paymentMethod = method.id;
    order.paymentMethodTitle = method.title;
    notifyListeners();
  }

  // its a list because of woocommerce's structure
  void setShippingMethod(ShippingMethod method) {
    order.shippingLines = [
      ShippingLine(methodId: method.id, methodTitle: method.title, total: 0)
    ];
    notifyListeners();
  }

  void setShippingAddress({
    String firstName,
    String lastName,
    String address1,
    String address2,
    String city,
    String state,
    String postcode,
    String country,
    String email,
    String phone,
  }) {
    order.shipping = order.shipping.copyWith(
        firstName: firstName,
        lastName: lastName,
        address1: address2,
        city: city,
        state: state,
        postcode: postcode,
        country: country,
        email: email,
        phone: phone);
    notifyListeners();
  }

  void setBillingAddress({
    String firstName,
    String lastName,
    String address1,
    String address2,
    String city,
    String state,
    String postcode,
    String country,
    String email,
    String phone,
  }) {
    order.billing = order.billing.copyWith(
        firstName: firstName,
        lastName: lastName,
        address1: address2,
        city: city,
        state: state,
        postcode: postcode,
        country: country,
        email: email,
        phone: phone);
    notifyListeners();
  }

  Future createOrder() async {
    isLoading = true;
    notifyListeners();
    try {
      order = await OrderApi.createOrder(order.copyWith(
          shipping: ShippingAddress.fromRawJson(getBilling.toRawJson())));
    } catch (e) {
      print(e.toString());
    }

    isLoading = false;
    notifyListeners();
  }

  Future updateOrder({Metadata metaData}) async {
    isLoading = true;
    notifyListeners();
    try {
      order = await OrderApi.updateOrder(order.copyWith(
          metaData: [...order.metaData, metaData],
          shipping: ShippingAddress.fromRawJson(getBilling.toRawJson()),
          lineItems: []));
    } catch (e) {
      print(e.toString());
    }
    isLoading = false;
    notifyListeners();
  }
}

class PaymentNotifier extends ChangeNotifier {
  bool isLoading;
  List<PaymentMethod> paymentMethods;
  PaymentNotifier() {
    paymentMethods = List<PaymentMethod>();
    getPaymentMethods();
  }
  Future<List<PaymentMethod>> getPaymentMethods() async {
    isLoading = true;
    notifyListeners();
    var res = await PaymentApi.listPayments();
    paymentMethods = res;
    isLoading = false;
    notifyListeners();
    return res;
  }
}

class ShippingNotifier extends ChangeNotifier {
  ShippingNotifier() {
    getShippingMethods();
  }

  bool isLoading;
  List<ShippingMethod> shippingMethods;
  Future<List<ShippingMethod>> getShippingMethods() async {
    isLoading = true;
    notifyListeners();
    var res = await ShippingApi.listShippingMethods();
    shippingMethods = res;
    isLoading = false;
    notifyListeners();
    return res;
  }
}

class PriceFormatter {
  static String format(dynamic price) {
    if (price is int) {
      price = price.toDouble().toStringAsFixed(2).toString();
    } else if (price is double) {
      price = price.toStringAsFixed(2).toString();
    }

    return "${Config.currency} $price";
  }

  static String formatPercent(dynamic regular, dynamic sale) {
    try {
      if (regular is String) {
        regular = double.parse(regular);
      } else if (regular is int) {
        regular = (regular as int).toDouble();
      }
      if (sale is String) {
        sale = double.parse(sale);
      } else if (sale is int) {
        sale = (regular as int).toDouble();
      }
      return "${((1 - (sale / regular)) * 100).toStringAsFixed(0)} %";
    } catch (e) {
      return "";
    }
  }
}
