import 'package:flutter/foundation.dart' hide Category;
import 'package:marketplace/data/data.dart';
import 'package:dartz/dartz.dart' hide Order;

class ProductsNotifier extends ChangeNotifier {
  bool isLoading;
  List<Product> _products;
  List<Product> get products => _products;
  ProductsNotifier({List<Product> products}) {
    _products = List<Product>();
    getProducts();
  }
  Future<List<Product>> getProducts({
    int page = 1,
    int perPage = 20,
    String search,
    List<int> include,
    List<int> exclude,
    String status = "any",
  }) async {
    isLoading = true;
    notifyListeners();
    try {
      List<Product> res = await ProductApi.listProducts(
        page: page,
        perPage: perPage,
        search: search,
        include: include,
        exclude: exclude,
        status: status,
      );
      isLoading = false;
      _products = res;
      notifyListeners();
      return res;
    } catch (e) {
      throw e;
    }
  }
}
