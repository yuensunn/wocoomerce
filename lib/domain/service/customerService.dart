import 'package:flutter/foundation.dart' hide Category;
import 'package:marketplace/data/data.dart';

class CustomerNotifier extends ChangeNotifier {
  bool isLoading;
  bool isGuest = false;
  Customer _customer;
  Customer get customer => _customer;
  Future getCustomerBySub(String id) async {
    isLoading = true;
    notifyListeners();
    Customer res = await CustomerApi.getCustomerBySub(id);
    _customer = res;
    isGuest = _customer == null;
    isLoading = false;
    notifyListeners();
  }
}
