import 'package:flutter/foundation.dart' hide Category;
import 'package:marketplace/data/data.dart';

class CategoriesNotifier extends ChangeNotifier {
  bool isLoading;
  List<Category> categories;
  CategoriesNotifier({this.categories = const []}) {
    getCategories();
  }
  Future getCategories({
    int page = 1,
    int perPage = 20,
    String search,
    List<int> include,
    List<int> exclude,
    String status = "any",
  }) async {
    isLoading = true;
    notifyListeners();
    try {
      categories = await ProductApi.listCategories(
        page: page,
        perPage: perPage,
        search: search,
        include: include,
        exclude: exclude,
        status: status,
      );
    } catch (e) {}
    isLoading = false;
    notifyListeners();
  }
}
