import 'package:marketplace/data/data.dart';

// Contain extra field to know if it is a digital item
class ExtendedLineItem extends LineItem {
  Product product;
  ProductVariant productVariant;
  bool virtual;

  String get id => "$productId$variationId";

  ExtendedLineItem(this.product, {ProductVariant variant, int q = 1}) {
    productId = product.id;
    quantity = q;
    if (product.type == ProductType.variable) {
      if (variant == null) throw ArgumentError.notNull("variant");

      productVariant = variant;
      variationId = variant.id;
    }
  }
}

// Contain extra field to know if it is a digital item
class ExtendedOrder {
  Order order;
  List<Product> product;
  List<Product> productVariant;

  ExtendedOrder(this.order);
}
