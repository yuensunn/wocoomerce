import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:marketplace/data/data.dart';
import 'package:marketplace/domain/service/service.dart';
import 'package:marketplace/presentation/productScreen.dart';

Future main() async {
  Provider.debugCheckInvalidValueType = null;
  runApp(MultiProvider(
    providers: [
      ChangeNotifierProvider(create: (context) => CustomerNotifier()),
      ChangeNotifierProvider(create: (context) => ProductsNotifier()),
      ChangeNotifierProvider(create: (context) => CategoriesNotifier()),
      ChangeNotifierProvider(create: (context) => CartNotifier()),
      ChangeNotifierProvider(create: (context) => AddressNotifier()),
      ChangeNotifierProvider(create: (context) => PaymentNotifier()),
      ChangeNotifierProvider(create: (context) => ShippingNotifier()),
      ChangeNotifierProvider(create: (context) => CategoriesNotifier()),
      ChangeNotifierProvider(create: (context) => OrdersNotifier())
    ],
    child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
            primarySwatch: Colors.blue,
            buttonTheme: ButtonThemeData(buttonColor: Colors.green),
            fontFamily: "AirbnbCereal",
            bottomSheetTheme: BottomSheetThemeData(
                backgroundColor: Colors.black.withOpacity(0.5))),
        home: App()),
  ));
}

class App extends StatefulWidget {
  App({Key key}) : super(key: key);
  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  JsonEncoder encoder = JsonEncoder.withIndent('  ');
  List<String> logs = List<String>();

  Widget _buildButton(String title, Function onPress) {
    return OutlineButton(
      onPressed: () {
        logs.clear();
        onPress();
      },
      color: Colors.green,
      child: Text(title),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Column(children: <Widget>[
        Container(
          height: 200,
          child: SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Wrap(
              direction: Axis.vertical,
              children: <Widget>[
                _buildButton("Go", () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => ProductScreen()));
                }),
                _buildButton("List products", () {
                  ProductApi.listProducts().then((res) {
                    setState(() {
                      logs.add(res
                          .map((x) => encoder.convert(x.toJson()))
                          .join("\n"));
                    });
                  }).catchError((e) {
                    logs.add(e.toString());
                  });
                }),
                _buildButton("Get 3454", () {
                  ProductApi.getProduct(3454).then((res) {
                    setState(() {
                      logs.add(encoder.convert(res.toJson()));
                    });
                  }).catchError((e) {
                    logs.add(e.toString());
                  });
                }),
                _buildButton("Get Variant 3454 ", () {
                  ProductApi.listVariants(3454).then((res) {
                    setState(() {
                      logs.add(res
                          .map((x) => encoder.convert(x.toJson()))
                          .join("\n"));
                    });
                  }).catchError((e) {
                    logs.add(e.toString());
                  });
                }),
                _buildButton("Get Attributes", () {
                  ProductApi.listAttributes().then((res) {
                    setState(() {
                      if (res.length > 0)
                        logs.add(res
                            .map((x) => encoder.convert(x.toJson()))
                            .join("\n"));
                      else
                        logs.add("empty");
                    });
                  }).catchError((e) {
                    logs.add(e.toString());
                  });
                }),
                _buildButton("list zone methods", () {
                  ShippingApi.listShippingZoneMethods().then((res) {
                    setState(() {
                      if (res.length > 0)
                        logs.add(res
                            .map((x) => encoder.convert(x.toJson()))
                            .join("\n"));
                      else
                        logs.add("empty");
                    });
                  }).catchError((e) {
                    logs.add(e.toString());
                  });
                }),
                _buildButton("list shipping methods", () {
                  ShippingApi.listShippingMethods().then((res) {
                    setState(() {
                      if (res.length > 0)
                        logs.add(res
                            .map((x) => encoder.convert(x.toJson()))
                            .join("\n"));
                      else
                        logs.add("empty");
                    });
                  }).catchError((e) {
                    logs.add(e.toString());
                  });
                }),
                _buildButton("list orders", () {
                  OrderApi.listOrders().then((res) {
                    setState(() {
                      if (res.length > 0)
                        logs.add(res
                            .map((x) => encoder.convert(x.toJson()))
                            .join("\n"));
                      else
                        logs.add("empty");
                    });
                  }).catchError((e) {
                    logs.add(e.toString());
                  });
                }),
                _buildButton("list categories", () {
                  ProductApi.listCategories().then((res) {
                    setState(() {
                      if (res.length > 0)
                        logs.add(res
                            .map((x) => encoder.convert(x.toJson()))
                            .join("\n"));
                      else
                        logs.add("empty");
                    });
                  }).catchError((e) {
                    logs.add(e.toString());
                  });
                }),
                _buildButton("Test", () {
                  print("hhh");
                })
              ],
            ),
          ),
        ),
        Expanded(
            child: ListView.separated(
          padding: EdgeInsets.symmetric(horizontal: 20),
          itemCount: logs.length,
          itemBuilder: (context, index) {
            return Text(logs[index]);
          },
          separatorBuilder: (context, index) {
            return Container(height: 30);
          },
        ))
      ]),
    );
  }
}
